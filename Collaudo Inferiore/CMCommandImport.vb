﻿'cmdOpen() 'OK
'cmdUnitCoverTest()
''cmdDeposit() 'OK
''cmdUndo() 'KO KO KO KO KO KO KO KO KO KO KO 
''CmdGetCashData() 'OK
''cmdGetVersion(0) 'OK
''CmdWithdrawal() 'KO KO KO KO KO KO KO KO KO 
'CmdExtendedStatus() 'OK
'cmdClose("R") 'OK
'cmdAssign() 'OK

Imports System
Imports System.Runtime.InteropServices
Imports System.Text
Imports Collaudo_Inferiore.CMClass
Imports Collaudo_Inferiore.frmPrincipale

Module CMCommandImport

    Public comCmdSend, comScopeSend, comCmdRec, comScopeRec As String
    Public hCon As Integer = 0
    Public hConAttuale As Integer = 0
    Public statoConnessione As String
    Public TransparentAnswer As String = ""
    Public comandoSingoloRisposta(120) As String

    'Costanti parametri per le strutture delle funzioni
    Public Const LENPASSWORD As Integer = 6
    Public Const MAXSLOTACTIVE As Integer = 4
    Public Const MAXFLASHREF As Integer = 16
    Public Const MAXCASSETTES As Integer = 24
    Public Const MAXDELAYCLASS As Integer = 10
    Public Const MAXUSERID As Integer = 10
    Public Const MAXMODULE As Integer = 30
    Public Const MAXCHARDENOM As Integer = 4
    Public Const MAXCHANNEL As Integer = 32
    Public Const MAXDEPOSIT As Integer = 200
    Public Const MAXDENOMINATION As Integer = 32
    Public Const MAXBOOKING As Integer = 29
    Public Const SERIALNUMBER As Integer = 12
    Public Const MAXLOG As Integer = 8192
    Public Const MAXHOOPERCOIN As Integer = 8
    Public Const NUMPROTDM As Integer = 6

    'Costanti per la struttura lpCmd delle chiamate alla DLL
    Public Const lpAUTOASSIGNCASSETTE As Byte = 65
    Public Const lpBELL As Byte = 66
    Public Const lpCLOSE As Byte = 67
    Public Const lpDEPOSITCASH As Byte = 68
    Public Const lpEXTENDEDSTATUS As Byte = 69
    Public Const lpFILL As Byte = 70
    Public Const lpGETCASHDATA As Byte = 71
    Public Const lpERRORLOG As Byte = 72
    Public Const lpINIT As Byte = 73
    Public Const lpJOURNAL As Byte = 74
    Public Const lpKEYCHANGE As Byte = 75
    Public Const lpDOWNLOAD As Byte = 76
    Public Const lpOPEN As Byte = 79
    Public Const lpSINGLECOMMAND As Byte = 91
    Public Const lpFILETRANSF As Byte = 94
    Public Const lpWITHDRAWAL As Byte = 87
    Public Const lpSETSEVLEVEL As Byte = 80
    Public Const lpSETOIDPAR As Byte = 78
    Public Const lpGETSEVLEVEL As Byte = 81
    Public Const lpGETCONFIG As Byte = 82
    Public Const lpSETCONFIG As Byte = 83
    Public Const lpTEST As Byte = 84
    Public Const lpUNDO As Byte = 85
    Public Const lpVERSION As Byte = 86
    Public Const lpTRANSPARENTSTART As Byte = 88
    Public Const lpTRANSPARENTEND As Byte = 89
    Public Const lpMODFILL As Byte = 90
    Public Const lpCTSTWS As Byte = 121
    Public Const lpGETCASHMONSTER As Byte = 103
    Public Const lpEXTSTATUSMONSTER As Byte = 101
    Public Const lpMOVENOTES As Byte = 119
    Public Const lpGETCNFMONSTER As Byte = 114
    Public Const lpSWAPNOTES As Byte = 115
    Public Const lpCOINDSP As Byte = 100
    Public Const lpUTILITY As Byte = 117
    Public Const lpROBBERY As Byte = 98
    Public Const lpMODDEP As Byte = 77
    Public Const lpEMULSEND As Byte = 93
    Public Const lpEMULRECEIVE As Byte = 64
    Public Const lpTRANSPARENT As Byte = 35
    Public Const lpTWSCTS As Byte = 123
    Public Const lpLEFTSIDE As Byte = 76
    Public Const lpRIGHTSIDE As Byte = 82
    'Public Const lpSINGLECONNECTION As Byte = 

    'Costanti di configurazione protocollo di connessione
    Public Const PROTTRANSPARENTOFF As Byte = 0
    Public Const PROTTRANSPARENTON As Byte = 1
    Public Const DLINKMODESERIAL As Byte = 83
    Public Const DLINKMODESERIALEASY As Byte = 115
    Public Const DLINKMODETCPIP As Byte = 76
    Public Const DLINKMODESSL As Byte = 108
    Public Const DLINKMODETLS As Byte = 116
    Public Const DLINKMODEUSB As Byte = 85
    Public Const DLINKMODEUSBEASY As Byte = 117
    Public Const DLINKMODEEMULATION As Byte = 69
    Public Const DLINKMODESERVERLAN As Byte = 101

    Public lstErrorLog As List(Of SErrorLog) = New List(Of SErrorLog)

    Structure SScope
        Dim Send As String
        Dim Receive As String
    End Structure
    Public myScope As New SScope


    Structure SMacchina
        Dim Status As MExtendedStatus
        Dim Open As SOpenStatus
        Dim Info As SInfo
        Dim Serials As SSerials
        Dim Firmware As SModuleFirmware
        Dim Test As STest
        Dim CashData As SCashData
        Dim ErrorLog As SErrorLog
    End Structure
    Public myMacchina As SMacchina = New SMacchina

    Public Class prova
        Public life As Single
        Public line As Integer
        Public replyCode As String
        Public operatorSide As String
        Public transaction As String
        Public statusFeed As String
        Public statusController As String
        Public statusReader As String
        Public statusSafe As String
        Public statusCassA As String
        Public statusCassB As String
        Public statusCassC As String
        Public statusCassD As String
        Public statusCassE As String
        Public statusCassF As String
        Public statusCassG As String
        Public statusCassH As String
        Public statusCassI As String
        Public statusCassJ As String
        Public statusCassK As String
        Public statusCassL As String
        Public statusCassM As String
        Public statusCassN As String
        Public statusCassO As String
        Public statusCassP As String
    End Class

    Structure SErrorLog
        Dim life As Single
        Dim line As Integer
        Dim replyCode As String
        Dim operatorSide As String
        Dim transaction As String
        Dim statusFeed As String
        Dim statusController As String
        Dim statusReader As String
        Dim statusSafe As String
        Dim statusCassA As String
        Dim statusCassB As String
        Dim statusCassC As String
        Dim statusCassD As String
        Dim statusCassE As String
        Dim statusCassF As String
        Dim statusCassG As String
        Dim statusCassH As String
        Dim statusCassI As String
        Dim statusCassJ As String
        Dim statusCassK As String
        Dim statusCassL As String
        Dim statusCassM As String
        Dim statusCassN As String
        Dim statusCassO As String
        Dim statusCassP As String
    End Structure
    Public errorLogLine As SErrorLog = New SErrorLog

    Structure SCliente
        Dim ID As Integer
        Dim Nome As String
    End Structure
    Structure SModuleFirmware
        Dim Suite As SFirmware
        Dim Reader As SFirmware
        Dim CassetteA As SFirmware
        Dim CassetteB As SFirmware
        Dim CassetteC As SFirmware
        Dim CassetteD As SFirmware
        Dim CassetteE As SFirmware
        Dim CassetteF As SFirmware
        Dim CassetteG As SFirmware
        Dim CassetteH As SFirmware
        Dim CassetteI As SFirmware
        Dim CassetteJ As SFirmware
        Dim CassetteK As SFirmware
        Dim CassetteL As SFirmware
        Dim BagA As SFirmware
        Dim CD80A As SFirmware
        Dim CD80B As SFirmware
    End Structure

    Structure SFirmware
        Dim Code As String
        Dim Name As String
        Dim Ver As String
        Dim Rel As String
    End Structure

    Structure SSerials
        Dim Reader As String
        Dim System As String
        Dim Controller As String
        Dim CassettoA As String
        Dim CassettoB As String
        Dim CassettoC As String
        Dim CassettoD As String
        Dim CassettoE As String
        Dim CassettoF As String
        Dim CassettoG As String
        Dim CassettoH As String
        Dim CassettoI As String
        Dim CassettoJ As String
        Dim CassettoK As String
        Dim CassettoL As String
        Dim Bag As String
    End Structure
    Structure SInfo
        Dim MACAddress As String
        Dim CassetteNumber As Integer
        Dim MachineCassetteNumber As Integer
        Dim Cliente As SCliente
        Dim Codice As String
        Dim CategoriaCompleta As String
        Dim Categoria As String
        Dim CRM As Integer
        Dim CD80 As Integer
        Dim Bag As Integer
        Dim replyCodeFile As String
        Dim cmConfig As String
        Dim cmOptionConfig As String
        Dim cmOptionOneConfig As String
        Dim cmOptionTwoConfig As String
    End Structure

    Structure SOpenStatus
        Dim Side As String
        Dim rc As String
    End Structure

    Structure MExtendedStatus
        Dim Reply As String
        Dim Feeder As String
        Dim Controller As String
        Dim Reader As String
        Dim Safe As String
        Dim CassetteA As String
        Dim CassetteB As String
        Dim CassetteC As String
        Dim CassetteD As String
        Dim CassetteE As String
        Dim CassetteF As String
        Dim CassetteG As String
        Dim CassetteH As String
        Dim CassetteI As String
        Dim CassetteJ As String
        Dim CassetteK As String
        Dim CassetteL As String
        Dim ProtocolCasseteNum As String
        Dim EscrowA As String
        Dim EscrowB As String
        Dim DepositA As String
        Dim DepositB As String
        Dim ProtocoloDepositNum As String
        Dim MsgFeeder As String
        Dim MsgController As String
        Dim MsgReader As String
        Dim MsgSafe As String
        Dim MsgCassetteA As String
        Dim MsgCassetteB As String
        Dim MsgCassetteC As String
        Dim MsgCassetteD As String
        Dim MsgCassetteE As String
        Dim MsgCassetteF As String
        Dim MsgCassetteG As String
        Dim MsgCassetteH As String
        Dim MsgCassetteI As String
        Dim MsgCassetteJ As String
        Dim MsgCassetteK As String
        Dim MsgCassetteL As String
        Dim MsgEscrowA As String
        Dim MsgEscrowB As String
        Dim MsgDepositA As String
        Dim MsgDepositB As String
    End Structure

    Public Structure ConnectionState
        Const Disconnect As String = "DISCONNESSO"
        Const Connect As String = "CONNESSO"
        Const Open As String = "APERTA"
        Const TransparentMode As String = "TRANSPARENTON"
        Const Errore As String = "ERRORE"
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    Structure SNull
    End Structure
    Public StrutturaNulla As SNull
    Dim dllError As New INIFile(localFolder & "\dllError.ini")

#Region "Vari Comandi singoli"
    Function LimitaCD80(Optional ByVal cassetteName As String = "A", Optional ByVal limite As Integer = 700) As String

        Dim comando As String = ""
        Dim stringa As String = ""
        LimitaCD80 = ""

        stringa = Hex(limite)
        For i = 1 To Len(stringa)
            comando &= "0" & Mid(stringa, i, 1)
        Next
        For i = Len(comando) To 7
            comando = "0" & comando
        Next
        Select Case cassetteName
            Case "A"
                comando = "9403" & comando
            Case "B"
                comando = "9503" & comando
        End Select

        CmdTransparentCommand(comando)
        LimitaCD80 = "OK"
    End Function

    Function LeggiSUITE() As String
        LeggiSUITE = cmdComandoSingolo("T,1,0,44", 4, 5000)
        Select Case LeggiSUITE
            Case "OK"
                myMacchina.Firmware.Suite.Name = comandoSingoloRisposta(5)
        End Select
    End Function

    Function LeggiSuiteFileName() As String
        LeggiSuiteFileName = cmdComandoSingolo("T,1,0,84", 4, 5000)
        Select Case LeggiSuiteFileName
            Case "OK"
                myMacchina.Firmware.Suite.Code = comandoSingoloRisposta(5)
            Case Else
                LeggiSuiteFileName = LeggiSUITE()
        End Select
    End Function

    Function SetCassetteNumber(ByVal cNumber As Integer) As String
        SetCassetteNumber = cmdComandoSingolo("F,1,11," & cNumber, 3, 5000)
        Select Case SetCassetteNumber
            Case "OK"
                myMacchina.Info.CassetteNumber = cNumber
        End Select
    End Function

    Function LeggiCassetteNumber() As String
        LeggiCassetteNumber = cmdComandoSingolo("T,1,0,88", 4, 5000)
        Select Case LeggiCassetteNumber
            Case "OK"
                myMacchina.Info.MachineCassetteNumber = comandoSingoloRisposta(9)
                myMacchina.Info.Bag = comandoSingoloRisposta(11)
                myMacchina.Info.CD80 = comandoSingoloRisposta(12)
            Case "SYNTAX ERROR", "SYNTAX ERROR OR COMMAND NOT AVAILABLE"
                LeggiCassetteNumber = cmdComandoSingolo("T,1,0,61", 4, 5000)
                myMacchina.Info.MachineCassetteNumber = comandoSingoloRisposta(5)
                myMacchina.Info.Bag = 0
                myMacchina.Info.CD80 = 0
                LeggiCassetteNumber = "OK"
        End Select
    End Function

    Function LeggiMACAddress() As String
        LeggiMACAddress = cmdComandoSingolo("T,1,6,14", 4, 5000)
        Select Case LeggiMACAddress
            Case "OK"
                myMacchina.Info.MACAddress = comandoSingoloRisposta(5)
        End Select
    End Function

    Function LeggiSerialeCassetti() As String
        For i = 1 To 12
            LeggiSerialeCassetti = cmdComandoSingolo("T,1," & Chr(64 + i) & ",13", 4, 5000)
            Select Case LeggiSerialeCassetti
                Case "OK"
                    Select Case i
                        Case 1
                            myMacchina.Firmware.CassetteA.Name = comandoSingoloRisposta(5)
                            myMacchina.Firmware.CassetteA.Ver = comandoSingoloRisposta(6)
                            myMacchina.Firmware.CassetteA.Rel = comandoSingoloRisposta(7)
                            myMacchina.Serials.CassettoA = comandoSingoloRisposta(8)
                        Case 2
                            myMacchina.Firmware.CassetteB.Name = comandoSingoloRisposta(5)
                            myMacchina.Firmware.CassetteB.Ver = comandoSingoloRisposta(6)
                            myMacchina.Firmware.CassetteB.Rel = comandoSingoloRisposta(7)
                            myMacchina.Serials.CassettoB = comandoSingoloRisposta(8)
                        Case 3
                            myMacchina.Firmware.CassetteC.Name = comandoSingoloRisposta(5)
                            myMacchina.Firmware.CassetteC.Ver = comandoSingoloRisposta(6)
                            myMacchina.Firmware.CassetteC.Rel = comandoSingoloRisposta(7)
                            myMacchina.Serials.CassettoC = comandoSingoloRisposta(8)
                        Case 4
                            myMacchina.Firmware.CassetteD.Name = comandoSingoloRisposta(5)
                            myMacchina.Firmware.CassetteD.Ver = comandoSingoloRisposta(6)
                            myMacchina.Firmware.CassetteD.Rel = comandoSingoloRisposta(7)
                            myMacchina.Serials.CassettoD = comandoSingoloRisposta(8)
                        Case 5
                            myMacchina.Firmware.CassetteE.Name = comandoSingoloRisposta(5)
                            myMacchina.Firmware.CassetteE.Ver = comandoSingoloRisposta(6)
                            myMacchina.Firmware.CassetteE.Rel = comandoSingoloRisposta(7)
                            myMacchina.Serials.CassettoE = comandoSingoloRisposta(8)
                        Case 6
                            myMacchina.Firmware.CassetteF.Name = comandoSingoloRisposta(5)
                            myMacchina.Firmware.CassetteF.Ver = comandoSingoloRisposta(6)
                            myMacchina.Firmware.CassetteF.Rel = comandoSingoloRisposta(7)
                            myMacchina.Serials.CassettoF = comandoSingoloRisposta(8)
                        Case 7
                            myMacchina.Firmware.CassetteG.Name = comandoSingoloRisposta(5)
                            myMacchina.Firmware.CassetteG.Ver = comandoSingoloRisposta(6)
                            myMacchina.Firmware.CassetteG.Rel = comandoSingoloRisposta(7)
                            myMacchina.Serials.CassettoG = comandoSingoloRisposta(8)
                        Case 8
                            myMacchina.Firmware.CassetteH.Name = comandoSingoloRisposta(5)
                            myMacchina.Firmware.CassetteH.Ver = comandoSingoloRisposta(6)
                            myMacchina.Firmware.CassetteH.Rel = comandoSingoloRisposta(7)
                            myMacchina.Serials.CassettoH = comandoSingoloRisposta(8)
                        Case 9
                            myMacchina.Firmware.CassetteI.Name = comandoSingoloRisposta(5)
                            myMacchina.Firmware.CassetteI.Ver = comandoSingoloRisposta(6)
                            myMacchina.Firmware.CassetteI.Rel = comandoSingoloRisposta(7)
                            myMacchina.Serials.CassettoI = comandoSingoloRisposta(8)
                        Case 10
                            myMacchina.Firmware.CassetteJ.Name = comandoSingoloRisposta(5)
                            myMacchina.Firmware.CassetteJ.Ver = comandoSingoloRisposta(6)
                            myMacchina.Firmware.CassetteJ.Rel = comandoSingoloRisposta(7)
                            myMacchina.Serials.CassettoJ = comandoSingoloRisposta(8)
                        Case 11
                            myMacchina.Firmware.CassetteK.Name = comandoSingoloRisposta(5)
                            myMacchina.Firmware.CassetteK.Ver = comandoSingoloRisposta(6)
                            myMacchina.Firmware.CassetteK.Rel = comandoSingoloRisposta(7)
                            myMacchina.Serials.CassettoK = comandoSingoloRisposta(8)
                        Case 12
                            myMacchina.Firmware.CassetteL.Name = comandoSingoloRisposta(5)
                            myMacchina.Firmware.CassetteL.Ver = comandoSingoloRisposta(6)
                            myMacchina.Firmware.CassetteL.Rel = comandoSingoloRisposta(7)
                            myMacchina.Serials.CassettoL = comandoSingoloRisposta(8)
                    End Select

            End Select
        Next
        LeggiSerialeCassetti = cmdComandoSingolo("T,1,A,13", 4, 5000)
        Select Case LeggiSerialeCassetti
            Case "OK"
                myMacchina.Serials.CassettoA = comandoSingoloRisposta(8)
        End Select
    End Function

    Function LeggiSerialeLettore() As String
        LeggiSerialeLettore = cmdComandoSingolo("T,1,3,13", 4, 5000)
        Select Case LeggiSerialeLettore
            Case "OK"
                myMacchina.Serials.Reader = comandoSingoloRisposta(8)
        End Select
    End Function
#End Region

#Region "Connection and Disconnection"
    ''' <summary>
    ''' Connetti
    ''' </summary>
    ''' <param name="hCon"></param>
    ''' <param name="connectionparam"></param>
    ''' <returns></returns>
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Connect")>
    Public Function Connetti(ByRef hCon As Int16, ByRef connectionparam As SConnectionParam) As Int16
    End Function

    ''' <summary>
    ''' Disconnetti
    ''' </summary>
    ''' <param name="hCon"></param>
    ''' <returns></returns>
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Disconnect")>
    Public Function Disconnetti(ByVal hCon As Int16) As Integer
    End Function

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    Structure SRSConf
        ''' <summary>
        ''' COM port name
        ''' </summary>
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=10)>
        Dim device As String
        ''' <summary>
        ''' Port speed
        ''' </summary>
        Dim baudrate As Int32
        ''' <summary>
        ''' Parity check type (0=no parity - 1=odd - 2=even)
        ''' </summary>
        Dim parity As Int32
        ''' <summary>
        ''' Stop bit number
        ''' </summary>
        Dim stopbit As Int32
        ''' <summary>
        ''' Data bits number
        ''' </summary>
        Dim car As Int32
        ''' <summary>
        ''' DTR managed by the CMDevice
        ''' </summary>
        Dim dtr As Boolean
    End Structure
    ''' <summary>
    ''' Parametri di connessione RS232
    ''' </summary>
    ''' <remarks></remarks>
    Public RsConf As SRSConf

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    Structure STcpIpPar
        ''' <summary>
        ''' CM IP address number or CM Netbios name
        ''' </summary>
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=20)>
        Dim clientIpAddr As String
        ''' <summary>
        ''' TCP IP comunication port number (8000-8009 / 8101 service port / 8100 used for file transfert)
        ''' </summary>
        Dim portNumber As Integer
    End Structure
    ''' <summary>
    ''' Parametri di connessione TCPIP
    ''' </summary>
    Public TcpIpPar As STcpIpPar

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    Structure SConnectionParam
        ''' <summary>
        ''' Connection type: S=RS232, s=RS232 simplified, L=LAN, 
        ''' l=LAN SSL, t=LAN TLS, U=USB, u=USB simplified, E=Emulation
        ''' </summary>
        Dim ConnectionMode As Byte
        ''' <summary>
        ''' Parametri di connessione RS232
        ''' </summary>
        Dim RsConf As SRSConf
        ''' <summary>
        ''' Parametri di connessione TCPIP
        ''' </summary>
        Dim TcpIpPar As STcpIpPar
    End Structure
    ''' <summary>
    ''' Parametri di connessione
    ''' </summary>
    Public ConnectionParam As SConnectionParam = New SConnectionParam
#End Region

#Region "Single Command"
    ''' <summary>
    ''' Single Command Import
    ''' </summary>
    ''' <param name="Hcon"></param>
    ''' <param name="idCommand"></param>
    ''' <param name="lpCmd"></param>
    ''' <param name="lpReply"></param>
    ''' <param name="timeOute"></param>
    ''' <returns></returns>
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function CmdSingleCommand(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As STransparent, ByRef lpReply As STransparent, ByVal timeOute As Int32) As Integer
    End Function

    ''' <summary>
    ''' Single Command Structure
    ''' </summary>
    Structure STransparent
        Dim buff As String
        Dim size As Integer
    End Structure
    Public myTransparent As STransparent = New STransparent
#End Region

#Region "Transparent Command"
    ''' <summary>
    ''' Invio comando in Transparent. E' necessario che la macchina sia in Transparent mode
    ''' </summary>
    ''' <param name="comando">Comando da inviare</param>
    ''' <param name="esadecimale">true se l'invio e la risposta sono in esadecimale. es. D2.03</param>
    ''' <returns></returns>
    Public Function CmdTransparentCommand(ByVal comando As String, Optional ByVal esadecimale As Boolean = False) As String
        Dim reply As Integer
        Dim risposta As String = ""
        Dim myTransparent As STransparent = New STransparent
        Dim myTranspRisp As STransparent = New STransparent

        If esadecimale = True Then
            'For i = 1 To Len(comando) Step 2
            '    Transparent.buff &= Chr(Hex("&H" & Mid(comando, i, 2)))
            'Next
            myTransparent.buff = Chr(comando)
        Else
            myTransparent.buff = comando
        End If

        myTransparent.size = Len(myTransparent.buff)
        myTranspRisp.buff = New String("&H00", 1700)
        myTranspRisp.size = Len(myTranspRisp.buff)
        'frmComScope.lstComScope.Items.Add(myTransparent.buff)
        frmPrincipale.ListBox1.Items.Add("SND:" & myTransparent.buff)
        reply = CmdSingleCommand(hCon, lpTRANSPARENT, myTransparent, myTranspRisp, 1000)
        If Len(myTranspRisp.buff) > 1500 Then
            myTranspRisp.buff = ""
        End If

        If esadecimale = True Then
            For i = 1 To Len(myTranspRisp.buff)
                risposta &= Hex(Asc(Mid(myTranspRisp.buff, i, 2)))
            Next
        Else
            risposta = myTranspRisp.buff
        End If
        CmdTransparentCommand = risposta
        TransparentAnswer = risposta
        'frmComScope.lstComScope.Items.Add(risposta)
        frmPrincipale.ListBox1.Items.Add("RCV:" & risposta)
        frmPrincipale.ListBox1.SelectedIndex = frmPrincipale.ListBox1.Items.Count - 1
    End Function
#End Region

    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLAdvanceUnitIdentification(ByVal Hcon As Int16, ByVal idCommand As Byte, ByVal lpCmd As SAdvanceUnitId, ByRef lpReply As SadvanceunitidReply, ByVal timeOute As Int16) As Integer
    End Function

    Structure SAdvanceUnitId
        Dim Modulo As Byte
        Dim Type As Byte
        'Dim ExInfo As SNull
    End Structure
    Public myAdvancedUnitId As SAdvanceUnitId

    Structure SadvanceunitidReply
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=6)>
        Dim CMxx As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim Name As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=11)>
        Dim Type As String
        Dim numProtCas As Int16
        Dim numCas As Int16
        Dim numProBag As Int16
        Dim numBag As Int16
        Dim numCd80 As Int16
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=11)>
        Dim Mode As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=11)>
        Dim Led As String
        Dim Rc As Int16
    End Structure
    Public myAdvanceUnitIdReply As SadvanceunitidReply

    Public Function cmdAdvanceUnitIdentification() As String
        cmdAdvanceUnitIdentification = ""
        MsgBox("La funzione Advance Unit Identification NON FUNZIONA!!!")
        Exit Function

        myAdvancedUnitId.Modulo = Convert.ToByte("0"c)
        myAdvancedUnitId.Type = CByte("88")

        cmdAdvanceUnitIdentification = DLLAdvanceUnitIdentification(hCon, lpTEST, myAdvancedUnitId, myAdvanceUnitIdReply, 5000)

        cmdUnitCoverTest()

    End Function


#Region "Assign cassette"
    ''' <summary>
    ''' Assign
    ''' </summary>
    ''' <param name="Hcon"></param>
    ''' <param name="idCommand"></param>
    ''' <param name="lpCmd"></param>
    ''' <param name="lpReply"></param>
    ''' <param name="timeOute"></param>
    ''' <returns></returns>
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLAssign(ByVal Hcon As Int16, ByVal idCommand As Byte, ByVal lpCmd As SNull, ByRef lpReply As SReplyAssign, ByVal timeOute As Int16) As Integer
    End Function

    ''' <summary>
    ''' Struttura per il reply code del comando Assign
    ''' </summary>
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    Structure SReplyAssign
        'Dim Part As Integer
        'Dim Delay_Id As Integer
        'Dim Act_slt As Integer
        'Dim CassNumber As Integer
        'Dim bn_in_low As Integer
        Dim Msg As Int16
        'Dim heat_st As Integer
        'Dim NumNotes As Integer
        'Dim Amount As Integer
        Dim rc As Long
    End Structure
    Public myAssignReply As SReplyAssign

    ''' <summary>
    ''' Comando Assign Cassette
    ''' </summary>
    ''' <returns></returns>
    Public Function cmdAssign() As String
        Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        cmdAssign = ""
        cmdAssign = DLLAssign(hCon, lpAUTOASSIGNCASSETTE, StrutturaNulla, myAssignReply, 5000)
        Select Case cmdAssign
            Case 0
                Select Case myAssignReply.Msg
                    Case 0
                        cmdAssign = replyCode.ReadValue("RC", myAssignReply.rc)
                    Case 1 To 10
                        cmdAssign = "Cassette " & Chr(myAssignReply.Msg + 55) & " NOT EMPTY"
                    Case 11 To 20
                        cmdAssign = "Cassette " & Chr(myAssignReply.Msg + 45) & " FAILED"
                End Select
            Case Else
                cmdAssign = dllError.ReadValue("dllAnswer", cmdAssign)
        End Select

    End Function
#End Region

#Region "Open"
    ''' <summary>
    ''' Open
    ''' </summary>
    ''' <param name="Hcon"></param>
    ''' <param name="idCommand"></param>
    ''' <param name="lpCmd"></param>
    ''' <param name="lpReply"></param>
    ''' <param name="timeOute"></param>
    ''' <returns></returns>
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLOpen(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SOpen, ByRef lpReply As SOpenReply, ByVal timeOute As Int32) As Integer
    End Function

    ''' <summary>
    ''' Invio comando di OPEN
    ''' </summary>
    ''' <param name="side">byte rappresentante la lettera del lato su cui effettuare la OPEN</param>
    ''' <param name="password">Password da utilizzare per la OPEN</param>
    ''' <returns></returns>
    Public Function cmdOpen(Optional ByRef side As String = "L", Optional ByRef password As String = "123456") As String
        Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        Dim bSide As Byte = Asc(side)
        cmdOpen = ""
        myOpen.Side = bSide
        myOpen.Password = password
        cmdOpen = DLLOpen(hCon, lpOPEN, myOpen, myOpenReply, 5000)
        Select Case cmdOpen
            Case 0
                cmdOpen = replyCode.ReadValue("RC", myOpenReply.Rc)
                If cmdOpen = "OK" Then
                    myMacchina.Open.Side = Chr(bSide)
                End If
            Case Else
                cmdOpen = dllError.ReadValue("dllAnswer", cmdOpen)
                myMacchina.Open.Side = "-"
        End Select
        myMacchina.Open.rc = cmdOpen
    End Function

    Public Function CMOpen(Optional ByRef side As String = "L", Optional ByRef password As String = "123456")
        Dim bSide As Byte = Asc(side)
        CMOpen = ""
        myOpen.Side = bSide
        myOpen.Password = password
        CMOpen = cmdComandoSingolo("O,1," & side & "," & password, 3, 5000)

    End Function

    '<StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    ''' <summary>
    ''' Struttura di invio comando OPEN
    ''' </summary>
    Structure SOpen
        ''' <summary>
        ''' Lato su cui eseguire la Open
        ''' </summary>
        Dim Side As Byte
        ''' <summary>
        ''' Password per eseguire la open
        ''' </summary>
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=7)> 'PASSWORD LEN + 1
        Dim Password As String
        ''' <summary>
        ''' Option: null=no parameter, 1=Get version of Boot loader FW, 2=Book side for 1 minute, 3=Cancel side booking
        ''' </summary>
        Dim Opt As IntPtr
    End Structure
    Public myOpen As SOpen = New SOpen

    ' <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi)>
    ''' <summary>
    ''' Struttura per il reply code del comando Open
    ''' </summary>
    Structure SOpenReply
        ''' <summary>
        ''' Lato su cui si è tentata la Open
        ''' </summary>
        Dim Side As Byte
        ''' <summary>
        ''' Fw Version
        ''' </summary>
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim FwVersion As String
        ''' <summary>
        ''' Reply code
        ''' </summary>
        Dim Rc As Int16
    End Structure
    Public myOpenReply As SOpenReply = New SOpenReply
#End Region

#Region "Close"
    ''' <summary>
    ''' Close
    ''' </summary>
    ''' <param name="Hcon"></param>
    ''' <param name="idCommand"></param>
    ''' <param name="lpCmd"></param>
    ''' <param name="lpReply"></param>
    ''' <param name="timeOute"></param>
    ''' <returns></returns>
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLClose(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SClose, ByRef lpReply As SCloseReply, ByVal timeOute As Int32) As Integer
    End Function

    ''' <summary>
    ''' Struttura di invio Close
    ''' </summary>
    Structure SClose
        ''' <summary>
        ''' Lato su cui eseguire la Close
        ''' </summary>
        Dim side As Byte
    End Structure
    Public myClose As SClose = New SClose

    ''' <summary>
    ''' Struttura per il reply del Close
    ''' </summary>
    Structure SCloseReply
        ''' <summary>
        ''' Lato su cui si è tentata la Close
        ''' </summary>
        Dim side As Byte
        ''' <summary>
        ''' Reply Code
        ''' </summary>
        Dim Rc As Int16
    End Structure
    Public myCloseReply As SCloseReply = New SCloseReply

    Public Function cmdClose(Optional ByVal side As String = "L") As String
        Dim bSide As Byte = Asc(side)
        Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        myClose.side = bSide
        cmdClose = DLLClose(hCon, lpCLOSE, myClose, myCloseReply, 5000)
        Select Case cmdClose
            Case 0
                cmdClose = replyCode.ReadValue("RC", myCloseReply.Rc)
            Case Else
                cmdClose = dllError.ReadValue("dllAnswer", cmdClose)
        End Select

    End Function

    Public Function CMClose(Optional ByVal side As String = "L") As String
        CMClose = cmdComandoSingolo("C,1," & side, 3, 5000)
    End Function

#End Region

#Region "Deposit"
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLDeposit(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SDeposit, ByRef lpReply As SDepositReply, ByVal timeOute As Int32) As Integer
    End Function

    Structure SDeposit
        Dim Side As Byte
        Dim Mode As Int16
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim Denom As String
        Dim exinfo As SNull
    End Structure
    Public myDeposit As SDeposit = New SDeposit

    Structure SDepositReply
        Dim Side As Byte
        Dim NotesToSave As Int16
        Dim NotesToOut As Int16
        Dim NotesToRej As Int16
        Dim fff As Int16 'Non utilizzato
        Dim NumItem As Int16
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=768)>
        Dim DepNote() As SInfoNotes
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=24)>
        Dim NoteInSafe() As Int16
        Dim rc As Int16
    End Structure
    Public myDepositReply As SDepositReply = New SDepositReply

    Structure SInfoNotes
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim Denom As String
        Dim Num As Int16
        'Dim mmm As Int16
    End Structure
    Public InfoNote As SInfoNotes = New SInfoNotes

    Public Function cmdDeposit(ByVal side As String, Optional ByVal denomination As String = "0000") As String
        Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        cmdDeposit = ""
        myDeposit.Side = Asc(side)
        myDeposit.Mode = 0
        myDeposit.Denom = denomination
        cmdDeposit = DLLDeposit(hCon, lpDEPOSITCASH, myDeposit, myDepositReply, 60000)
        Select Case cmdDeposit
            Case 0
                cmdDeposit = replyCode.ReadValue("RC", myDepositReply.rc)
            Case Else
                cmdDeposit = dllError.ReadValue("dllAnswer", cmdDeposit)
        End Select
    End Function

    Public Function CMDeposit(ByVal side As String, Optional ByVal denomination As String = "0000") As String
        Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        CMDeposit = ""
        myDeposit.Side = Asc(side)
        myDeposit.Mode = 0
        myDeposit.Denom = denomination
        CMDeposit = cmdComandoSingolo("D,1," & side & ",0," & denomination, 3, 60000)

    End Function
#End Region

#Region "Unit Cover Test"
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLUnitCoverTest(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SInpOpTest, ByRef lpReply As SInpOpTestReply, ByVal timeOute As Int32) As Integer
    End Function

    Structure SInpOpTest
        Dim Modulo As Byte
        Dim Type As Byte
    End Structure
    Public InpOpTest As SInpOpTest = New SInpOpTest

    Structure SInpOpTestReply
        Dim SafeDoor As Int16 '0=closed - 1=open
        Dim TrayCassette As Int16 '0=closed - 1=open
        Dim Cover As Int16 '0=closed - 1=open
        Dim Feeder As Int16 '0=closed - 1=open
        Dim InputSlot As Int16 '0=empty - 1=notes detected
        Dim RejectSlot As Int16 '0=empty - 1=notes detected
        Dim LeftSlot As Int16 '0=empty - 1=notes detected
        Dim RightSlot As Int16 '0=empty - 1=notes detected
        Dim LeftExtButton As Int16
        Dim RightExtButton As Int16
        Dim CageOpen As Int16
        Dim GateOpen As Int16
        Dim BagOpen As Int16
        Dim Fkb As Int16
        Dim Fks As Int16
        Dim Rc As Int16
    End Structure
    Public lpInpOpTestReply As SInpOpTestReply = New SInpOpTestReply

    Public Function cmdUnitCoverTest(Optional ByVal denomination As String = "0000") As String
        Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        InpOpTest.Modulo = Convert.ToByte("0"c)
        InpOpTest.Type = Convert.ToByte("0"c)

        cmdUnitCoverTest = DLLUnitCoverTest(hCon, lpTEST, InpOpTest, lpInpOpTestReply, 5000)
        Select Case cmdUnitCoverTest
            Case 0
                cmdUnitCoverTest = replyCode.ReadValue("RC", lpInpOpTestReply.Rc)
            Case Else
                cmdUnitCoverTest = dllError.ReadValue("dllAnswer", cmdUnitCoverTest)
        End Select
    End Function
#End Region

#Region "Withdrawal"
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLWithdrawal(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SWithdrawal, ByRef lpReply As SWithdrawalReply, ByVal timeOute As Int32) As Integer
    End Function

    Structure SWithdrawal
        Dim Side As Byte
        'Dim Target As Int16
        Dim NumItem As Int16
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=24)>
        Dim Info() As SInfoNotes
    End Structure
    Public myWithdrawal As SWithdrawal = New SWithdrawal

    Structure SWithdrawalReply
        Dim Side As Byte
        Dim NumItem As Int16
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=24)>
        Dim Info() As SInfoNotes
        Dim rc As Int16
    End Structure
    Public myWithdrawalReply As SWithdrawalReply = New SWithdrawalReply

    Public Function CmdWithdrawal() As String
        Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        CmdWithdrawal = ""
        myWithdrawal.Side = lpRIGHTSIDE
        myWithdrawal.NumItem = 1
        ReDim myWithdrawal.Info(myWithdrawal.NumItem - 1)
        myWithdrawal.Info(0).Denom = "AAAA"
        myWithdrawal.Info(0).Num = 8
        CmdWithdrawal = DLLWithdrawal(hCon, lpWITHDRAWAL, myWithdrawal, myWithdrawalReply, 112000)

        Select Case CmdWithdrawal
            Case 0
                CmdWithdrawal = replyCode.ReadValue("RC", myWithdrawalReply.rc)
            Case Else
                CmdWithdrawal = dllError.ReadValue("dllAnswer", CmdWithdrawal)
        End Select
    End Function

#End Region ' NON FUNZIONA!!!

#Region "Undo Deposit"
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLUndo(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SUndo, ByRef lpReply As SWithdrawalReply, ByVal timeOute As Int32) As Integer
    End Function

    Structure SUndo
        Dim Side As Byte
        Dim Mode As Int16
    End Structure
    Public myUndo As SUndo = New SUndo

    Public Function cmdUndo(Optional ByVal mode As String = "") As String
        Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")

        myUndo.Side = lpRIGHTSIDE
        myUndo.Mode = 0
        cmdUndo = DLLUndo(hCon, lpUNDO, myUndo, myWithdrawalReply, 112000)
        Select Case cmdUndo
            Case 0
                cmdUndo = replyCode.ReadValue("RC", myWithdrawalReply.rc)
            Case Else
                cmdUndo = dllError.ReadValue("dllAnswer", cmdUndo)
        End Select
    End Function

#End Region 'FUNZIONA MA NON LEGGE IL REPLY CODE

#Region "Get Version"
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLGetVersion(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SGetVersion, ByRef lpReply As SGetVersionReply, ByVal timeOute As Int32) As Integer
    End Function

    Structure SGetVersion
        Dim target As Byte
    End Structure
    Public myGetVersion As SGetVersion = New SGetVersion

    Structure SGetVersionReply
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim Ver As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim Rel As String
        Dim rc As Int16
    End Structure
    Public myGetVersionReply As SGetVersionReply = New SGetVersionReply

    ''' <summary>
    ''' Comando per leggere la versione e la release del firmware dei singoli moduli
    ''' </summary>
    ''' <param name="target">Modulo da interrogare(0:MainControllerCompatibility-1:Feeder - 2:MainController - 3:Reader - 4:SafeController - 5:FPGA - 6:OSC - A..L:Cassetti</param>
    ''' <returns></returns>
    Public Function cmdGetVersion(ByVal target As String)
        Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        cmdGetVersion = ""
        myGetVersion.target = Asc(target)
        cmdGetVersion = DLLGetVersion(hCon, lpVERSION, myGetVersion, myGetVersionReply, 5000)

        Select Case cmdGetVersion
            Case 0
                cmdGetVersion = replyCode.ReadValue("RC", myGetVersionReply.rc)
            Case Else
                cmdGetVersion = dllError.ReadValue("dllAnswer", cmdGetVersion)
        End Select

    End Function
#End Region

#Region "Extended Status"
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLExtendedStatus(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SNull, ByRef lpReply As SExtendedStatusReply, ByVal timeOute As Int32) As Integer
    End Function

    Structure SExtendedStatusReply
        Dim ErrorTable As SErrorTable
        Dim rc As Int16
    End Structure
    Public myExtendedStatus As SExtendedStatusReply = New SExtendedStatusReply

    Structure SErrorTable
        Dim NumItem As Int16
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=54)> '30
        Dim Info() As SInfoErr
    End Structure

    Structure SInfoErr
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim err As String
    End Structure

    Public Function CmdExtendedStatus() As String
        Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        CmdExtendedStatus = ""

        CmdExtendedStatus = DLLExtendedStatus(hCon, lpEXTENDEDSTATUS, StrutturaNulla, myExtendedStatus, 5000)

        Select Case CmdExtendedStatus
            Case 0
                CmdExtendedStatus = replyCode.ReadValue("RC", myExtendedStatus.rc)
            Case Else
                CmdExtendedStatus = dllError.ReadValue("dllAnswer", CmdExtendedStatus)
        End Select
        If CmdExtendedStatus = "OK" Then
            For i = 0 To myExtendedStatus.ErrorTable.NumItem
                If myExtendedStatus.ErrorTable.Info(i).err.ToString = "" Then Exit For
                Select Case Mid(myExtendedStatus.ErrorTable.Info(i).err, 1, 1)
                    Case "1"
                        myMacchina.Status.Feeder = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgFeeder = replyCode.ReadValue("Feeder", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "2"
                        myMacchina.Status.Controller = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgController = replyCode.ReadValue("Controller", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "3"
                        myMacchina.Status.Safe = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgSafe = replyCode.ReadValue("Safe", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "4"
                        myMacchina.Status.Reader = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgReader = replyCode.ReadValue("Reader", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "A"
                        myMacchina.Status.CassetteA = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteA = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "B"
                        myMacchina.Status.CassetteB = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteB = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "C"
                        myMacchina.Status.CassetteC = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteC = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "D"
                        myMacchina.Status.CassetteD = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteD = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "E"
                        myMacchina.Status.CassetteE = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteE = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "F"
                        myMacchina.Status.CassetteF = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteF = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "G"
                        myMacchina.Status.CassetteG = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteG = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "H"
                        myMacchina.Status.CassetteH = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteH = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "I"
                        myMacchina.Status.CassetteI = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteI = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "J"
                        myMacchina.Status.CassetteJ = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteJ = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "K"
                        myMacchina.Status.CassetteK = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteK = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "L"
                        myMacchina.Status.CassetteL = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgCassetteL = replyCode.ReadValue("Cassettes", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "q"
                        myMacchina.Status.EscrowA = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgEscrowA = replyCode.ReadValue("Bags", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "r"
                        myMacchina.Status.EscrowB = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgEscrowB = replyCode.ReadValue("Bags", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "a"
                        myMacchina.Status.DepositA = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgDepositA = replyCode.ReadValue("Bags", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                    Case "b"
                        myMacchina.Status.DepositB = Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2)
                        myMacchina.Status.MsgDepositB = replyCode.ReadValue("Bags", Mid(myExtendedStatus.ErrorTable.Info(i).err.ToString, 2, 2))
                End Select
            Next
        End If

    End Function

    Public Function CMExtendedStatus() As String
        Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        CMExtendedStatus = ""

        CMExtendedStatus = cmdComandoSingolo("E,1", 2, 5000)

        If CMExtendedStatus = "OK" Then
            For i = 4 To comandoSingoloRisposta.Length - 1
                If comandoSingoloRisposta(i) = "" Then Exit For
                Select Case Mid(comandoSingoloRisposta(i), 1, 1)
                    Case "1"
                        myMacchina.Status.Feeder = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgFeeder = replyCode.ReadValue("Feeder", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "2"
                        myMacchina.Status.Controller = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgController = replyCode.ReadValue("Controller", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "3"
                        myMacchina.Status.Safe = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgSafe = replyCode.ReadValue("Safe", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "4"
                        myMacchina.Status.Reader = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgReader = replyCode.ReadValue("Reader", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "A"
                        myMacchina.Status.CassetteA = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgCassetteA = replyCode.ReadValue("Cassettes", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "B"
                        myMacchina.Status.CassetteB = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgCassetteB = replyCode.ReadValue("Cassettes", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "C"
                        myMacchina.Status.CassetteC = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgCassetteC = replyCode.ReadValue("Cassettes", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "D"
                        myMacchina.Status.CassetteD = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgCassetteD = replyCode.ReadValue("Cassettes", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "E"
                        myMacchina.Status.CassetteE = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgCassetteE = replyCode.ReadValue("Cassettes", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "F"
                        myMacchina.Status.CassetteF = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgCassetteF = replyCode.ReadValue("Cassettes", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "G"
                        myMacchina.Status.CassetteG = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgCassetteG = replyCode.ReadValue("Cassettes", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "H"
                        myMacchina.Status.CassetteH = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgCassetteH = replyCode.ReadValue("Cassettes", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "I"
                        myMacchina.Status.CassetteI = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgCassetteI = replyCode.ReadValue("Cassettes", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "J"
                        myMacchina.Status.CassetteJ = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgCassetteJ = replyCode.ReadValue("Cassettes", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "K"
                        myMacchina.Status.CassetteK = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgCassetteK = replyCode.ReadValue("Cassettes", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "L"
                        myMacchina.Status.CassetteL = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgCassetteL = replyCode.ReadValue("Cassettes", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "q"
                        myMacchina.Status.EscrowA = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgEscrowA = replyCode.ReadValue("Bags", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "r"
                        myMacchina.Status.EscrowB = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgEscrowB = replyCode.ReadValue("Bags", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "a"
                        myMacchina.Status.DepositA = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgDepositA = replyCode.ReadValue("Bags", Mid(comandoSingoloRisposta(i), 2, 2))
                    Case "b"
                        myMacchina.Status.DepositB = Mid(comandoSingoloRisposta(i), 2, 2)
                        myMacchina.Status.MsgDepositB = replyCode.ReadValue("Bags", Mid(comandoSingoloRisposta(i), 2, 2))
                End Select
            Next
        End If

    End Function

#End Region

#Region "Get Cach Data"
    <DllImport("CMCommand.dll", CallingConvention:=CallingConvention.Winapi, CharSet:=CharSet.Ansi, SetLastError:=True, ExactSpelling:=True, EntryPoint:="CMCommand_Execute")>
    Public Function DLLGetCashData(ByVal Hcon As Int16, ByVal idCommand As Byte, ByRef lpCmd As SGetCashData, ByRef lpReply As SGetCashDataReply, ByVal timeOute As Int32) As Integer
    End Function

    Structure SGetCashData
        Dim side As Byte
    End Structure
    Public myGetCashData As SGetCashData = New SGetCashData

    Structure SGetCashDataReply
        Dim Side As Byte
        Dim NumItem As Int16
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=768)>
        Dim Info() As SInfoCassettes
        Dim rc As Int16
    End Structure
    Public myGetCashDataReply As SGetCashDataReply = New SGetCashDataReply

    Structure SInfoCassettes
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=5)>
        Dim Denom As String
        Dim NotesNum As Integer
        Dim NumFree As Int16
        Dim Cassette As Byte
        Dim Enable As Byte
    End Structure

    Public Function CmdGetCashData() As String
        Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")
        CmdGetCashData = ""
        myGetCashData.side = lpRIGHTSIDE
        CmdGetCashData = DLLGetCashData(hCon, lpGETCASHDATA, myGetCashData, myGetCashDataReply, 5000)

        Select Case CmdGetCashData
            Case 0
                CmdGetCashData = replyCode.ReadValue("RC", myGetCashDataReply.rc)
            Case Else
                CmdGetCashData = dllError.ReadValue("dllAnswer", CmdGetCashData)
        End Select

    End Function

#End Region

    ''' <summary>
    ''' Chiude la connessione attiva
    ''' </summary>
    ''' <returns></returns>
    Public Function ChiudiConnessione() As Boolean
        ChiudiConnessione = False
        Dim replyDisconnect As Integer
        replyDisconnect = Disconnetti(hCon)
        Select Case replyDisconnect
            Case 0
                statoConnessione = ConnectionState.Disconnect
            Case Else
        End Select
    End Function

    ''' <summary>
    ''' Esegue un Single Command, la risposta completa viene memorizzata nella matrice comandoSingoloRisposta
    ''' </summary>
    ''' <param name="comando">Comando completo. es X,1</param>
    ''' <param name="RCindex">Indice della posizione del replyCode</param>
    ''' <returns></returns>
    Public Function cmdComandoSingolo(ByVal comando As String, Optional RCindex As Integer = 4, Optional ByVal timeOut As Integer = 5000) As String
        cmdComandoSingolo = ""
        Dim risposta As String = ""
        Dim reply As Integer
        Dim mySingleCommand As STransparent = New STransparent
        Dim ReplySingleCommand As STransparent = New STransparent
        Dim inizio, lunghezza, posizione As Integer
        ReDim comandoSingoloRisposta(120)
        Dim replyCode As New INIFile(localFolder & "\TheRCs" & myMacchina.Info.replyCodeFile & ".ini")

        mySingleCommand.buff = comando
        mySingleCommand.size = Len(mySingleCommand.buff)
        ReplySingleCommand.buff = New String("&H00", 1700)
        ReplySingleCommand.size = Len(ReplySingleCommand.buff)
        frmPrincipale.ListBox1.Items.Add("SND:" & comando)
        'frmComScope.lstComScope.Items.Add("SND: " & comando)
        reply = CmdSingleCommand(hCon, lpSINGLECOMMAND, mySingleCommand, ReplySingleCommand, timeOut)
        risposta = ReplySingleCommand.buff
        frmPrincipale.ListBox1.Items.Add("RCV:" & risposta)
        'frmComScope.lstComScope.Items.Add("RCV: " & risposta)
        'frmComScope.lstComScope.SelectedIndex = frmComScope.lstComScope.Items.Count - 1
        'frmComScope.Refresh()
        posizione = 1
        inizio = 1
        lunghezza = 1
        'For i = 0 To comandoSingoloRisposta.GetUpperBound(0) - 1
        '    comandoSingoloRisposta(i) = ""
        'Next
        'For i = 1 To Len(ReplySingleCommand.buff)
        '    If Mid(ReplySingleCommand.buff, i, 1) = "," Then
        '        lunghezza = i - inizio
        '        comandoSingoloRisposta(posizione) = Mid(ReplySingleCommand.buff, inizio, lunghezza)
        '        inizio = inizio + lunghezza + 1
        '        posizione += 1
        '    End If
        'Next
        comandoSingoloRisposta = Split(ReplySingleCommand.buff, ",")
        'comandoSingoloRisposta(posizione) = Mid(ReplySingleCommand.buff, inizio, Len(ReplySingleCommand.buff) - inizio + 1)
        If comandoSingoloRisposta.Length = 1 Then
            risposta = ""
        Else
            cmdComandoSingolo = replyCode.ReadValue("RC", comandoSingoloRisposta(RCindex))
            If cmdComandoSingolo = "Failed" Then cmdComandoSingolo = comandoSingoloRisposta(RCindex)
        End If
        'Then

        '    If InStr(risposta, "&") > 0 Then
        '    risposta = ""
        'End If
    End Function

    ''' <summary>
    ''' Prova la connessione impostata
    ''' </summary>
    ''' <returns></returns>
    Public Function ProvaConnessione() As Boolean
        Dim replyConnection As Integer
connessione:
        If statoConnessione = ConnectionState.Connect Then
            ChiudiConnessione()
        End If
        ProvaConnessione = False
        replyConnection = Connetti(hCon, ConnectionParam)
        Select Case replyConnection
            Case 0, 1052 '1052=Connessione già attiva
                If hCon > hConAttuale Then hConAttuale = hCon
                If cmdComandoSingolo("T,1,0,88", 4) = "OK" Then
                    ProvaConnessione = True
                    statoConnessione = ConnectionState.Connect
                    myMacchina.Test.CassetteNumber = comandoSingoloRisposta(10)
                End If
            Case 1050

            Case Else
                statoConnessione = ConnectionState.Errore
                'ScriviMessaggio(String.Format(messaggio(201), replyConnection), lblMessage, ArcaColor.errore) 'Problemi di connessione. Errore numero: xx. Premere un tasto per ripetere, ESC per uscire
                Select Case AttendiTasto()
                    Case "ESC"
                        Exit Function
                    Case Else
                        GoTo connessione
                End Select
        End Select

    End Function

    Public Function ImpostaConnessione() As Boolean
        ImpostaConnessione = False
        ChiudiTutteConnessioni()
        ImpostaConnessione = ApriConnessione()

        'Select Case modalità
        '    Case "RS232"
        '        For Each rsPort As String In My.Computer.Ports.SerialPortNames
        '            With ConnectionParam
        '                .ConnectionMode = DLINKMODESERIAL
        '                .RsConf.baudrate = 9600
        '                .RsConf.parity = 0
        '                .RsConf.car = 8
        '                .RsConf.stopbit = 1
        '                .RsConf.dtr = False
        '                .RsConf.device = rsPort
        '            End With
        '            ImpostaConnessione = ProvaConnessione()
        '            If ImpostaConnessione = True Then Exit For
        '        Next
        '    Case "USB"
        '        ConnectionParam.ConnectionMode = DLINKMODEUSB
        '        ImpostaConnessione = ApriConnessione()
        '    Case "LAN"
        '        ConnectionParam.ConnectionMode = DLINKMODETCPIP
        '        If Len(hostName) > 0 Then
        '            ConnectionParam.TcpIpPar.clientIpAddr = hostName
        '        Else
        '            ConnectionParam.TcpIpPar.clientIpAddr = configIni.ReadValue("CMconfig", "CMIPAddress")
        '        End If
        '        If portNumber > 0 Then
        '            ConnectionParam.TcpIpPar.portNumber = portNumber
        '        Else
        '            ConnectionParam.TcpIpPar.portNumber = configIni.ReadValue("CMconfig", "CMLanPort")
        '        End If
        '        ImpostaConnessione = ApriConnessione()
        'End Select

    End Function

    ''' <summary>
    ''' Chiude tutte le connessioni aperte
    ''' </summary>
    ''' <returns></returns>
    Public Function ChiudiTutteConnessioni() As Boolean
        ChiudiTutteConnessioni = False
    End Function



    ''' <summary>
    ''' Apre la connessione impostata
    ''' </summary>
    ''' <returns></returns>
    Public Function ApriConnessione() As Boolean
        Dim replyConnection As Integer
        Dim numTentativi As Integer = 1
        Dim risposta As String = ""
connessione:
        'If statoConnessione = ConnectionState.Connect Then
        ChiudiConnessione()
        'End If
        ApriConnessione = False
        replyConnection = Connetti(hCon, ConnectionParam)
        Select Case replyConnection
            Case 0, 1052 '1052=Connessione già attiva
                If hCon > hConAttuale Then hConAttuale = hCon
                statoConnessione = ConnectionState.Connect
            Case 1052 'Connessione già attiva

                'Case 1056 'Error on TCP/IP socket

            Case Else
                statoConnessione = ConnectionState.Errore
                If MsgBox(String.Format(Messaggio(242), replyConnection), MsgBoxStyle.RetryCancel, "CONNECTION FAILED") = MsgBoxResult.Retry Then
                    GoTo connessione
                End If
        End Select
        risposta = cmdComandoSingolo("T,1,0,88", 4)
        If risposta = "OK" Or risposta = "101" Or risposta = "1" Then
            ApriConnessione = True
            GoTo endCommand
        Else
            Select Case numTentativi
                Case 1
                    ConnectionParam.ConnectionMode = DLINKMODESERIAL
                Case 2
                    ConnectionParam.ConnectionMode = DLINKMODESERIALEASY
                Case 3
                    ConnectionParam.ConnectionMode = DLINKMODEUSB
                Case 4
                    ConnectionParam.ConnectionMode = DLINKMODEUSBEASY
            End Select
            If numTentativi > 4 Then GoTo endCommand
            numTentativi += 1
            GoTo connessione
        End If

endCommand:
        Return ApriConnessione
    End Function



    '********************   CM TRACE    ****************

    'Structure STraceDirective
    '    Dim InfoLayer As slayer
    '    Dim TracePathName As Byte()
    '    Dim lpComScope As Scomscope
    '    Dim TrcType As Strctype
    '    Dim TrcSize As Int16
    '    Dim TrcNum As Int16
    '    Dim sinfocomp As STRComponent
    'End Structure
    'Public TraceDirective As New STraceDirective

    'Structure STRComponent
    '    Dim Level As Int16
    'End Structure

    'Structure SComScope
    '    Dim hWin As hwnd
    'End Structure
End Module

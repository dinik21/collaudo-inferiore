﻿Module Hi_lev
    Sub HiLevSend(ByVal Send$, Optional ByVal OUT As String = "", Optional ByVal download_suite As Boolean = False)
        Aggiorna_list_com("SND: " & Send$)
        If moRS232.IsOpen = False Then moRS232.Open(Comm_Port, 9600, 8, Rs232.DataParity.Parity_None, Rs232.DataStopBit.StopBit_1, 1024)
        No_Timeout = True
        Ricez$ = ""
        Dim Style As Integer = vbYesNo + vbCritical + vbDefaultButton3 ' Define buttons.
        Dim Title As String = "Errore comunicazione"  ' Define title.
        Dim msg As String = "'Connessione fallita. Riprovare?" ' mex(295) ' Define message.'Connessione fallita. Riprovare?

        OUT = Chr(&H2) + Chr(&H31) + Send$ + Chr(&H3)

        Dim limite As DateTime = DateTime.Now.AddMilliseconds(1800)

        Dim S = 0
        Dim D = 0
        Dim n = Len(OUT$)

        For aaa = 2 To n Step 1
            S = (Asc(Mid$(OUT, aaa, 1)))
            D = (D + S)
        Next
        Dim ck As Byte = (D And &H7F)
        OUT = OUT + Chr(ck)

aa:
        'If DateTime.Now > limite Then
        '    Dim response As Byte = MsgBox(msg, Style, Title)
        '    Select Case response
        '        Case 6 : GoTo aa
        '        Case 7 : GoTo fine
        '    End Select
        'End If

        Dim dl As Byte = 0
        OKcomm = False

        Call Traspsend(Chr(5), 2, 2500) 'invio "ENQ"

        If Controllo_Risposta("1030") = False Then
            If download_suite = True Then GoTo aa
            Dim response As Byte = MsgBox(msg, Style, Title)
            Select Case response
                Case 6 : GoTo aa
                Case 7 : GoTo fine
            End Select

        End If

        Call Traspsend((OUT$), 2, 2500)


        If Controllo_Risposta("1031") = False Then
            If download_suite = True Then GoTo aa
            Dim response As Byte = MsgBox(msg, Style, Title)
            Select Case response
                Case 6 : GoTo aa
                Case 7 : GoTo fine
            End Select

        End If

        Call Traspsend(Chr(4), 1, 450000) 'invio "ENQ"

        ''************** RISPOSTA ***********************

        If Controllo_Risposta("05") = False Then
            If download_suite = True Then GoTo aa
            Dim response As Byte = MsgBox(msg, Style, Title)
            Select Case response
                Case 6 : GoTo aa
                Case 7 : GoTo fine
            End Select

        End If

        Ricez = ""
Dl0:

        If dl = 0 Then
            Call Traspsend(Chr(&H10) & Chr(&H30), 0, 0)
            dl = 1
        Else
            Call Traspsend(Chr(&H10) & Chr(&H31), 0, 0)
            dl = 0
        End If

        Call Traspsend("", 1, 2500)
        Dim Rx As String = stringa_ricezione(Traspric)
        Dim Fine_Loop As Boolean
        limite = DateTime.Now.AddMilliseconds(2500)
        Dim Errore_Trasmissione As Boolean
        Errore_Trasmissione = False
        Fine_Loop = False
        Do While Not Fine_Loop = True
            Call Traspsend("", 1, 100, True)
            Dim last_byte As String = stringa_ricezione(Traspric)
            Rx = Rx & last_byte

            If last_byte = "" Or last_byte = "" Then
                Fine_Loop = True
                Errore_Trasmissione = True
                Exit Do
            End If

            If Len(Rx) > 3 Then
                If Mid$(Rx, Len(Rx) - 3, 2) = "03" Or Mid$(Rx, Len(Rx) - 3, 2) = "17" Then
                    Fine_Loop = True
                    GoTo salta_controllo
                End If
            End If

            If last_byte = "05" Or last_byte = "04" Then
                Fine_Loop = True
                Errore_Trasmissione = True
                Exit Do
            End If

salta_controllo:
            If DateTime.Now > limite Then
                Fine_Loop = True
                Errore_Trasmissione = True
            End If
        Loop
        Dim aa As String = stringa_invio(Rx)
        If Errore_Trasmissione = True Then
            If download_suite = True Then GoTo aa
            Dim response As Byte = MsgBox(msg, Style, Title)
            Select Case response
                Case 6 : GoTo aa
                Case 7 : GoTo fine
            End Select

        End If

        D = 0
        S = 0
        n = Len(Rx)

        For aaa = 3 To n - 3 Step 2
            S = ("&h" & (Mid$(Rx, aaa, 2)))
            D = (D + S)
        Next

        Dim CRC = (D And &H7F)

        Dim crc_mex As Integer

        crc_mex = "&h" & Right(Rx, 2)
        If crc_mex <> CRC Then
            If download_suite = True Then GoTo aa
            Dim response As Byte = MsgBox(msg, Style, Title)
            Select Case response
                Case 6 : GoTo aa
                Case 7 : GoTo fine
            End Select

        End If
        Dim kk As String = Mid$(Rx, Len(Rx) - 3, 2)
        If Mid$(Rx, Len(Rx) - 3, 2) = "17" Then
            Ricez = Ricez + Mid$(Rx, 5, Len(Rx) - 8)
            GoTo Dl0
        Else
            Ricez = Ricez + Mid$(Rx, 5, Len(Rx) - 8)
        End If

        Ricez = stringa_invio(Ricez)

        If dl = 0 Then
            Call Traspsend(Chr(&H10) & Chr(&H30), 1, 2500)
            dl = 1
        Else
            Call Traspsend(Chr(&H10) & Chr(&H31), 1, 2500)
            dl = 0
        End If

        If Controllo_Risposta("04") = False Then
            If download_suite = True Then GoTo aa
            Dim response As Byte = MsgBox(msg, Style, Title)
            Select Case response
                Case 6 : GoTo aa
                Case 7 : GoTo fine
            End Select

        End If

        OKcomm = True


fine:
        If moRS232.IsOpen = True Then moRS232.Close()
        No_Timeout = False
        Aggiorna_list_com("RCV: " & Ricez)
    End Sub

    Function Controllo_Risposta(ByVal Risposta As String) As Boolean
        Controllo_Risposta = True
        If stringa_ricezione(Traspric) <> Risposta Then

            If Len(Traspric) = 0 Then 'Controllo che ci sia stata la risposta

                Controllo_Risposta = False

            End If

            If stringa_ricezione(Traspric) = "15" Then 'Controllo che la risposta non sia BUSY

                Controllo_Risposta = False

            End If
        Else

            Controllo_Risposta = True

        End If

    End Function

    Public Function hex_asc(ByVal stringa)
        hex_asc = ""
        For a = 1 To Len(stringa) Step 2
            hex_asc = hex_asc + Chr("&h" + (Mid$(stringa, a, 2)))
        Next

    End Function

    Public Function hex_dec(ByVal stringa)
        hex_dec = ""
        For a = 1 To Len(stringa) Step 2
            hex_dec = hex_dec + Str(Val("&h" + (Mid$(stringa, a, 2)))) & " "
        Next

    End Function

End Module


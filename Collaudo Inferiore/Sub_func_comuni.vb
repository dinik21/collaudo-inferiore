﻿Imports System.Math
Imports System.IO

Structure Banc_cas
    Dim nome As String
    Dim N_note As Integer
    Dim Denomination As String
End Structure

Structure _bottone
    Dim Titolo As String
    Dim Comando As String
End Structure

Structure _checklist
    Dim Titolo As String
    Dim Stringa As String
    Dim Button1 As _bottone
    Dim Button2 As _bottone
    Dim Button3 As _bottone
    Dim Button4 As _bottone
    Dim comando_uscita As String
End Structure


Structure limiti_safe

    Dim F_Cash_L_min As Integer
    Dim F_Cash_L_max As Integer
    Dim F_Cash_R_min As Integer
    Dim F_Cash_R_max As Integer

End Structure

Structure _foto_safe
    Dim Limiti As limiti_safe
    Dim F_Cash_L As Integer
    Dim F_Cash_R As Integer

End Structure

Structure limiti_cassette

    Dim F_in_min As Integer
    Dim F_in_max As Integer
    Dim F_out_min As Integer
    Dim F_out_max As Integer
    Dim F_In_Box_min As Integer
    Dim F_In_Box_max As Integer

End Structure

Structure _cassette_photo
    Dim Limiti As limiti_cassette
    Dim Foto_in As Byte
    Dim Foto_Out As Byte
End Structure

Structure _test
    Dim test As Boolean
    Dim photo_sensor As Boolean
    Dim cassette_photo() As _cassette_photo
    Dim Bn_riconosciute As Integer
    Dim Bn_rifiutate As Integer
    Dim RiduzioneCap As Boolean
    Dim FwCD80 As String

End Structure
Structure _inferiore

    Dim categoria As String
    Dim Matricola_Inferiore As String
    Dim Codice_cliente As Integer
    Dim Nome_cliente As String
    Dim Codice_Inferiore As String
    Dim Codice_Macchina As String
    Dim Matricola_Controller As String
    Dim Matricola_lettore As String
    Dim Codice_FW_Suite As String
    Dim Codice_FW_castomization As String
    Dim MatricolaCassA As String
    Dim MatricolaCassB As String
    Dim CD80_Matricola_A As String
    Dim CD80_Matricola_B As String
    Dim CD80_Matricola_C As String
    Dim CD80_Matricola_D As String
    Dim Data As String
    Dim Ora As String
    Dim CRM As String
    Dim CD80 As Integer
    Dim assign As Boolean
    Dim Cassette_number As Boolean
    Dim test_cd80 As Boolean
    Dim setup_cassette As Boolean
    Dim Deposito As Boolean
    'Dim Codice_FW_lettore As String
    'Dim Codice_Fw_riferimenti As String
    Dim Test As _test
End Structure

Module Sub_func_comuni
    Structure _colore
        Dim Normal As Color
        Dim Warning As Color
        Dim Ok As Color
        Dim Errore As Color
    End Structure

    Structure _cd80_status

        Dim Switch_lock_A As Boolean
        Dim Switch_lock_B As Boolean
        Dim Vassoio As Boolean
        Dim Status_A As String
        Dim Status_B As String
        Dim Mag_sens_A As Boolean
        Dim Mag_sens_B As Boolean
        Dim Sorter_fork_A As Boolean
        Dim Sorter_fork_B As Boolean

    End Structure
    Public CD80_status As _cd80_status
    Public Inferiore As _inferiore
    Public Colore As _colore
    Public Traspric As String
    Public Salva_com As Boolean
    Public Debug_test As Boolean
    Public Debug_pausa As Integer
    Public WorkDir As String
    Public No_Timeout As Boolean = False
    Public Tasto As Integer
    Public k_timeout As Integer
    Public Baudrate As Integer
    Public Const PURGE_RXCLEAR As Integer = &H8
    Public WithEvents moRS232 As Rs232
    Private Delegate Sub CommEventUpdate(ByVal source As Rs232, ByVal mask As Rs232.EventMasks)
    Public Stringa_vuota As Boolean
    Public Stringa_di_connessione As String
    Public no_timeout_USB As Boolean
    Public Tipo_Comunicazione As Byte
    Public Ricez As String
    Public mex() As String
    Public OKcomm As Boolean
    Public IP_Macchina As String
    Public IP_Old As String
    Public Database_path As String
    Public Database_name As String
    Public Primo_avvio As Boolean
    Public Fw_Path As String
    Public Transparent_ON As Boolean
    Public Zip_path As String
    Public Comm_Port As Integer
    Public Numero_cassetti_in_test As Byte

    'Public photo_status As _foto_status

    Public Function stringa_invio(ByVal stringa As String) As String
        stringa_invio = ""
        For a = 0 To Len(stringa) - 1 Step 2

            stringa_invio = stringa_invio + Chr("&h" + stringa.Substring(a, 2))
        Next

    End Function


    Public Function stringa_ricezione(ByVal stringa As String) As String
        Dim aaa As String
        stringa_ricezione = ""
        If stringa <> "" Then
            For a = 1 To Len(stringa)
                aaa = Hex$(Asc(Mid$(stringa, a, 1)))
                If Len(aaa) = 1 Then aaa = "0" + aaa
                stringa_ricezione = stringa_ricezione + aaa
            Next
        End If

    End Function

    Public Sub Scrivi_N(ByVal Stringa As String)
        frmPrincipale.Label1.ForeColor = Colore.Normal
        frmPrincipale.Label1.Text = Stringa
        Application.DoEvents()
    End Sub

    Public Sub Scrivi_E(ByVal Stringa As String)
        frmPrincipale.Label1.ForeColor = Colore.Errore
        frmPrincipale.Label1.Text = Stringa
        Application.DoEvents()
    End Sub

    Public Sub Scrivi_O(ByVal Stringa As String)
        frmPrincipale.Label1.ForeColor = Colore.Ok
        frmPrincipale.Label1.Text = Stringa
        Application.DoEvents()
    End Sub

    Public Sub Scrivi_W(ByVal Stringa As String)
        frmPrincipale.Label1.ForeColor = Colore.Warning
        frmPrincipale.Label1.Text = Stringa
        Application.DoEvents()
    End Sub

    Public Sub MySleep(ByVal Millisec As Integer)

        Dim iIndex As Int16
        For iIndex = 1 To Millisec / 10

            Threading.Thread.Sleep(10)
            Application.DoEvents()

        Next

    End Sub

    Public Function METTE_0(ByVal pippo As String)
        Dim stringa As String
        stringa = ""

        For a = 1 To Len(pippo)

            stringa = stringa + "0" + Mid$(pippo, a, 1)

        Next

        METTE_0 = stringa

    End Function

    Function dec_hex(ByVal DEC As Byte) As String

        If DEC.ToString <> "" Then

            dec_hex = Hex(DEC) : If Len(dec_hex) = 1 Then dec_hex = "0" & dec_hex
            dec_hex = dec_hex
        Else
            dec_hex = ""

        End If

    End Function
    Function dec_hex(ByVal DEC As Int16) As String

        If DEC.ToString <> "" Then
            dec_hex = Hex(DEC)
            For a = Len(dec_hex) + 1 To 4
                dec_hex = "0" & dec_hex
            Next
        Else
            dec_hex = ""

        End If

    End Function
    Function dec_hex(ByVal DEC As UInt16) As String

        If DEC.ToString <> "" Then
            dec_hex = Hex(DEC)
            For a = Len(dec_hex) + 1 To 4
                dec_hex = "0" & dec_hex
            Next
        Else
            dec_hex = ""

        End If

    End Function

    Function dec_hex(ByVal DEC As Int32) As String

        If DEC.ToString <> "" Then
            dec_hex = Hex(DEC)
            For a = Len(dec_hex) + 1 To 8
                dec_hex = "0" & dec_hex
            Next
        Else
            dec_hex = ""

        End If

    End Function
    Function TOGLIE_0(ByVal pippo As String)
        Dim stringa As String = ""
        For a = 2 To Len(pippo) Step 2

            stringa = stringa + Mid$(pippo, a, 1)

        Next

        TOGLIE_0 = stringa

    End Function


    Public Function hex_bin(ByVal hex As String)
        Dim b As String
        hex_bin = ""
        For a = 1 To Len(hex)

            b = dec_bin("&h" + Mid$(hex, a, 1))
            b = Mid(b, 5)

            hex_bin = hex_bin + b
        Next
        Dim k As Integer
        Select Case hex.Length
            Case 1
                k = 4
            Case 2
                k = 8
            Case 3
                k = 16
            Case 4
                k = 16
        End Select

        For a = 1 To k - Len(hex_bin)

            hex_bin = "0" + Trim(hex_bin)
        Next

    End Function

    Public Function bin_dec(ByVal bin)
        Dim aa As Long = 0
        For a = 0 To Len(bin) - 1
            Application.DoEvents()
            aa = aa + Val(Mid$(bin, Len(bin) - a, 1) * 2 ^ a)
        Next

        bin_dec = aa

    End Function
    Public Function dec_bin(ByVal dec)
        Dim aa As Integer
        On Error Resume Next
        dec_bin = ""
        Do While Not dec = 0
            aa = dec \ 2

            If aa * 2 = Val(dec) Then
                dec_bin = "0" + dec_bin
            Else
                dec_bin = "1" + dec_bin
            End If
            dec = Int(dec / 2)
        Loop

        If dec_bin = "" Then dec_bin = "0"

        For a = 1 To 8 - Len(dec_bin)

            dec_bin = "0" + Trim(dec_bin)
        Next

    End Function

    Public Function int_offset(ByVal st As String) As Integer

        If Mid$(st, 2, 1) = "0" Then
            int_offset = -(Val("&h" + Mid$(st, 3, 2)))
        Else
            int_offset = (Val("&h" + Mid$(st, 3, 2)))
        End If

    End Function

    Function Carattere_pad(ByVal stringa As String, ByVal carattere As Char, ByVal num_tot_caratteri As Byte) As String

        Carattere_pad = stringa

        For a = stringa.Length To num_tot_caratteri - 1
            Carattere_pad = carattere & Carattere_pad
        Next

    End Function
    Public Function bin_hex(ByVal bin)
        Dim p
        bin_hex = ""
        If bin = "0" Then bin_hex = "0" : Exit Function
        Do While Not Len(bin) Mod 4 = 0
            bin = "0" + bin
            Application.DoEvents()
        Loop

        For a = 1 To Len(bin) Step 4
            Application.DoEvents()
            p = Hex(bin_dec(Mid$(bin, a, 4)))
            bin_hex = Trim((bin_hex + p))
        Next

    End Function
    Public Sub Attendi_Invio()

        frmPrincipale.Focus()
        Tasto = 0
        Do While Not Tasto = 13
            Application.DoEvents()
            Threading.Thread.Sleep(50)
        Loop

    End Sub

    Sub Attendi_Tasto()

        frmPrincipale.Focus()
        Tasto = 0
        Do While Not Tasto <> 0
            Application.DoEvents()
            Threading.Thread.Sleep(50)
        Loop

    End Sub

    Public Sub Traspsend(ByVal Out As String, ByVal NByte As Integer, ByVal Tout As Integer, Optional ByVal Hi_lev As Boolean = False)
        If Transparent_ON = True Then Aggiorna_list_com("SND: " & Out)
        If Transparent_ON = True Then
            If moRS232.IsOpen = False Then moRS232.Open(Comm_Port, 9600, 8, Rs232.DataParity.Parity_None, Rs232.DataStopBit.StopBit_1, 1024)
        End If

        Traspric = ""
        '----------------------
        '// Clear Tx/Rx Buffers
        If Hi_lev = False Then moRS232.PurgeBuffer(Rs232.PurgeBuffers.TxClear Or Rs232.PurgeBuffers.RXClear)
        moRS232.RxBufferThreshold = Int32.Parse(NByte)
        moRS232.Timeout = Val(Tout)
        If Transparent_ON = True Then NByte = NByte * 2
        moRS232.Write(Out)

        '// Clears Rx textbox
        Dim aa = moRS232.StopBit
        If NByte > 0 Then
            Ricezione(NByte)
        Else
            MySleep(Tout)
        End If

        If Transparent_ON = True Then
            If moRS232.IsOpen = True Then moRS232.Close()
        End If

        If Transparent_ON = True And Traspric <> "" Then Aggiorna_list_com("RCV: " & Traspric)
    End Sub

    Private Sub Ricezione(ByVal nbyte As Integer)

        Dim Risp As Integer = moRS232.Read(Int32.Parse(nbyte))

        If Risp = 0 Then
            Traspric = ""
        Else
            Traspric = moRS232.InputStreamString.ToString
        End If


    End Sub
    Private Sub moRS232_CommEvent(ByVal source As Rs232, ByVal Mask As Rs232.EventMasks) Handles moRS232.CommEvent
        '===================================================
        '												©2003 www.codeworks.it All rights reserved
        '
        '	Description	:	Events raised when a comunication event occurs
        '	Created			:	15/07/03 - 15:13:46
        '	Author			:	Corrado Cavalli
        '
        '						*Parameters Info*
        '
        '	Notes				:	
        '===================================================
        Debug.Assert(frmPrincipale.InvokeRequired = False)

        Dim iPnt As Int32, Buffer() As Byte
        Debug.Assert(frmPrincipale.InvokeRequired = False)

        If (Mask And Rs232.EventMasks.RxChar) > 0 Then

            Buffer = source.InputStream
            For iPnt = 0 To Buffer.Length - 1

            Next

        End If

    End Sub

#Region "UI update routine"
#End Region



    Sub attendi_per(ByVal stato As String)
        Traspric = Chr("&H" + stato)
        Do While Not Traspric <> Chr("&H" + stato)
            Call Traspsend("F3", 1, 1000)
            If Mid$(Traspric, 1, 1) = Chr(0) Then
                Traspric = Chr("&H" + stato)
            End If
            MySleep(100)

        Loop

    End Sub
    Function attendi_per_t(ByVal stato As String, ByVal lop As Long, ByVal t As Integer) As Boolean

        Traspric = Chr("&H" & stato)
        Dim limite As DateTime = DateTime.Now.AddMilliseconds(t)
        Do While Not Traspric <> Chr("&H" & stato)
            If DateTime.Now > limite Then
                attendi_per_t = False

                Exit Function
            End If
            Call Traspsend("F3", 1, 1000)
            If Mid$(Traspric, 1, 1) = Chr(0) Then
                Traspric = Chr("&H" & stato)
            End If
            MySleep(lop)
            Application.DoEvents()
        Loop
        If Traspric = "" Then attendi_per_t = False : Exit Function

        attendi_per_t = True

    End Function




    Sub Traspin()

        If Inferiore.categoria <> "CIMA" Then
ripeti:
            Transparent_ON = False
            Dim Send As String = "X,1"
            'Call HiLevSend(Send)
            cmdComandoSingolo("X,1", 2)

            If OKcomm = False Then

            Else
                'If Right(Ricez$, 2) = ",4" Or Right(Ricez$, 3) = "104" Then
                If comandoSingoloRisposta(2) = "4" Or comandoSingoloRisposta(2) = "104" Then
                    OKcomm = False
                Else
                    OKcomm = True
                    Transparent_ON = True
                End If

            End If
        Else


        End If
    End Sub
    Sub Traspout()

        Transparent_ON = False
        Dim Send As String = "Y,1"

        'Call HiLevSend(Send)
        cmdComandoSingolo("Y,1", 2)
        If OKcomm = False Then
            Transparent_ON = True
            Exit Sub

        Else
            Transparent_ON = False
        End If

    End Sub


    Private Sub close_port()

        If moRS232.IsOpen = True Then moRS232.Close()

    End Sub

    Function USB_Array_to_String(ByVal buf As Byte()) As String
        USB_Array_to_String = ""
        If buf.Length > 0 Then
            For a = 0 To buf.Length - 1
                USB_Array_to_String = USB_Array_to_String & Chr(buf(a))
            Next
        End If
    End Function

    Sub Power_On()
        Traspsend("D233", 1, 1000)
    End Sub

    Sub Power_Off()
        Traspsend("D234", 1, 1000)
    End Sub

    Sub MSO_Imput_on()
        Traspsend("D210", 1, 1000)
    End Sub

    Sub MSO_Output_on()
        Traspsend("D211", 1, 1000)
    End Sub

    Sub Pressor_Home()
        Traspsend("D20B", 1, 3000)
    End Sub

    Sub Pressor_Opened()
        Traspsend("D20C", 1, 3000)
    End Sub

    Sub Pressor_Closed()
        Traspsend("D20D", 1, 3000)
    End Sub

    Sub Prefeeding_On()
        Traspsend("D20E", 1, 3000)
    End Sub

    Sub Prefeeding_Off()
        Traspsend("D20F", 1, 3000)
    End Sub

    Sub Allign_Home()
        Traspsend("D21C", 1, 3000)
    End Sub

    Sub Push_Home()
        Traspsend("D21D", 1, 3000)
    End Sub

    Sub Push_Open()
        Traspsend("D21F", 1, 3000)
    End Sub

    Function Preset() As String
        Scrivi_N("Preset in corso attendere..")
        Call Traspsend("83", 1, 10000)
        Preset = stringa_ricezione(Traspric)

    End Function

    'Function Deposito_ALL(Optional ByVal counting As Boolean = False) As Boolean

    '    Call Principale.Open_R(True)
    '    If counting = False Then
    '        Call HiLevSend("D,1,R,0,0000", "")
    '    Else
    '        Call HiLevSend("D,1,R,3,0000", "")
    '    End If

    '    Call Interpreta_deposito(Ricez$)
    '    Deposito_ALL = True

    'End Function



    Function Setup_cassette(ByVal obj As Object) As Boolean

        Dim Stringa_Setup As String

        Scrivi_N("Setup cassetti lato L in corso....") 'Setup cassetti lato L in corso....
        frmPrincipale.Close_R()

        If frmPrincipale.Open_L(False) = False Then
            Setup_cassette = False
            Exit Function
        End If

        Select Case Inferiore.categoria
            Case "CM18B"
                Stringa_Setup = "S,1,L,CPCA,Y,A,CPDA,Y,B,CPEA,Y,C,CPGA,Y,D,CPHA,Y,E,CP**,Y,F"
            Case "CM18", "CM14", "CM24", "CM24B", "CM20S", "CM18EVO"
                If Inferiore.CD80 = 0 Then
                    Stringa_Setup = "S,1,L,CPCA,Y,A,CPDA,Y,B,CPEA,Y,C,CPGA,Y,D,CPGA,Y,E,CPHA,Y,F,CPIA,Y,G,CPJA,Y,H"
                Else
                    If Inferiore.CD80 = 2 Then
                        Stringa_Setup = "S,1,L,CPCA,Y,C,CPDA,Y,D,CPGA,Y,E,CPHA,Y,F,CPIA,Y,G,CPJA,Y,H"
                    End If
                    If Inferiore.CD80 = 4 Then
                        Stringa_Setup = "S,1,L,CPCA,Y,E,CPDA,Y,F,CPIA,Y,G,CPJA,Y,H"
                    End If
                End If

            Case "CM18T", "CM18EVOT", "CM20T"
                If Inferiore.CD80 = 0 Then
                    Stringa_Setup = "S,1,L,CPCA,Y,A,CPDA,Y,B,CPEA,Y,C,CPGA,Y,D,CPGA,Y,E,CPHA,Y,F,CPIA,Y,G,CPJA,Y,H,CPCA,Y,I,CPDA,Y,J,CPEA,Y,K,CPHA,Y,L"
                Else
                    If Inferiore.CD80 = 2 Then
                        Stringa_Setup = "S,1,L,CPCA,Y,C,CPDA,Y,D,CPGA,Y,E,CPHA,Y,F,CPIA,Y,G,CPJA,Y,H,CPCA,Y,I,CPDA,Y,J,CPEA,Y,K,CPHA,Y,L"
                    End If
                    If Inferiore.CD80 = 4 Then
                        Stringa_Setup = "S,1,L,CPCA,Y,E,CPDA,Y,F,CPEA,Y,G,CPGA,Y,H,CPHA,Y,I,CPIA,Y,J,CPJA,Y,K,CPCA,Y,L"

                    End If
                End If

            Case "CM20"
                If Inferiore.CD80 = 0 Then
                    Stringa_Setup = "S,1,L,CPCA,Y,A,CPDA,Y,B,CPEA,Y,C,CPGA,Y,D,CPGA,Y,E,CPHA,Y,F,CPIA,Y,G,CPJA,Y,H,CPCA,Y,I,CPDA,Y,J"
                Else
                    If Inferiore.CD80 = 2 Then
                        Stringa_Setup = "S,1,L,CPCEA,Y,C,CPDA,Y,D,CPEA,Y,E,CPGA,Y,F,CPHA,Y,G,CPIA,Y,H,CPJA,Y,I,CPHA,Y,J"
                    End If
                    If Inferiore.CD80 = 4 Then
                        Stringa_Setup = "S,1,L,CPCA,Y,E,CPDA,Y,F,CPEA,Y,G,CPGA,Y,H,CPIA,Y,I,CPJA,Y,J"

                    End If
                End If
        End Select

        'Call HiLevSend(Stringa_Setup, "")
        cmdComandoSingolo(Stringa_Setup, 3, 60000)
        'If Right(Ricez$, 2) = ",0" Or Right(Ricez$, 3) = "101" Then
        If comandoSingoloRisposta(3) = "1" Or comandoSingoloRisposta(3) = "101" Then
            Setup_cassette = True
        Else
            Setup_cassette = False
            frmPrincipale.Close_L()

            Exit Function
        End If

        Scrivi_N("Setup cassetti lato R in corso....") 'Setup cassetti lato R in corso....

        frmPrincipale.Close_L()

        If frmPrincipale.Open_R(False) = False Then
            Setup_cassette = False
            Exit Function
        End If

        Select Case Inferiore.categoria
            Case "CM18B"
                Stringa_Setup = "S,1,R,CPCA,Y,A,CPDA,Y,B,CPEA,Y,C,CPGA,Y,D,CPHA,Y,E,CP**,Y,F"
            Case "CM18", "CM14", "CM24", "CM24B", "CM20S", "CM18EVO"
                If Inferiore.CD80 = 0 Then
                    Stringa_Setup = "S,1,R,CPCA,Y,A,CPDA,Y,B,CPEA,Y,C,CPGA,Y,D,CPGA,Y,E,CPHA,Y,F,CPIA,Y,G,CPJA,Y,H"
                Else
                    If Inferiore.CD80 = 2 Then
                        Stringa_Setup = "S,1,R,CPCA,Y,C,CPDA,Y,D,CPGA,Y,E,CPHA,Y,F,CPIA,Y,G,CPJA,Y,H"
                    End If
                    If Inferiore.CD80 = 4 Then
                        Stringa_Setup = "S,1,R,CPCA,Y,E,CPDA,Y,F,CPIA,Y,G,CPJA,Y,H"
                    End If
                End If

            Case "CM18T", "CM18EVOT", "CM20T"
                If Inferiore.CD80 = 0 Then
                    Stringa_Setup = "S,1,R,CPCA,Y,A,CPDA,Y,B,CPEA,Y,C,CPGA,Y,D,CPGA,Y,E,CPHA,Y,F,CPIA,Y,G,CPJA,Y,H,CPCA,Y,I,CPDA,Y,J,CPEA,Y,K,CPHA,Y,L"
                Else
                    If Inferiore.CD80 = 2 Then
                        Stringa_Setup = "S,1,R,CPCA,Y,C,CPDA,Y,D,CPGA,Y,E,CPHA,Y,F,CPIA,Y,G,CPJA,Y,H,CPCA,Y,I,CPDA,Y,J,CPEA,Y,K,CPHA,Y,L"
                    End If
                    If Inferiore.CD80 = 4 Then
                        Stringa_Setup = "S,1,R,CPCA,Y,E,CPDA,Y,F,CPEA,Y,G,CPGA,Y,H,CPHA,Y,I,CPIA,Y,J,CPJA,Y,K,CPCA,Y,L"

                    End If
                End If

            Case "CM20"
                If Inferiore.CD80 = 0 Then
                    Stringa_Setup = "S,1,R,CPCA,Y,A,CPDA,Y,B,CPEA,Y,C,CPGA,Y,D,CPGA,Y,E,CPHA,Y,F,CPIA,Y,G,CPJA,Y,H,CPCA,Y,I,CPDA,Y,J"
                Else
                    If Inferiore.CD80 = 2 Then
                        Stringa_Setup = "S,1,R,CPCEA,Y,C,CPDA,Y,D,CPEA,Y,E,CPGA,Y,F,CPHA,Y,G,CPIA,Y,H,CPJA,Y,I,CPHA,Y,J"
                    End If
                    If Inferiore.CD80 = 4 Then
                        Stringa_Setup = "S,1,R,CPCA,Y,E,CPDA,Y,F,CPEA,Y,G,CPGA,Y,H,CPIA,Y,I,CPJA,Y,J"

                    End If
                End If
        End Select
        'Call HiLevSend(Stringa_Setup, "")
        cmdComandoSingolo(Stringa_Setup, 3,60000)
        'If Right(Ricez$, 2) = ",0" Or Right(Ricez$, 3) = "101" Then
        If comandoSingoloRisposta(3) = "1" Or comandoSingoloRisposta(3) = "101" Then
            Setup_cassette = True
        Else
            Setup_cassette = False
        End If

        Scrivi_N("Abilitazione tagli") 'Abilitazione tagli

        'Call HiLevSend("S,5,R,CPCA,Y,3,CPDA,Y,3,CPEA,Y,3,CPGA,Y,3,CPHA,Y,3,CPIA,Y,3,CPJA,Y,3", "")
        cmdComandoSingolo("S,5,R,CPCA,Y,3,CPDA,Y,3,CPEA,Y,3,CPGA,Y,3,CPHA,Y,3,CPIA,Y,3,CPJA,Y,3", 3)
        'If Right(Ricez$, 2) = ",0" Or Right(Ricez$, 3) = "101" Then
        If comandoSingoloRisposta(3) = "1" Or comandoSingoloRisposta(3) = "101" Then
            Setup_cassette = True
        Else
            Setup_cassette = False

        End If
    End Function


    Public Sub Aggiorna_list_com(ByVal stringa As String)

        If frmPrincipale.ListBox1.Items.Count = 10 Then
            For a = 1 To 9
                frmPrincipale.ListBox1.Items.Item(a - 1) = frmPrincipale.ListBox1.Items.Item(a)
            Next
            frmPrincipale.ListBox1.Items.Item(9) = stringa
        Else
            frmPrincipale.ListBox1.Items.Add(stringa)
        End If

    End Sub
End Module




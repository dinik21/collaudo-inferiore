﻿Imports System.Data.OleDb
Imports System.IO

Public Class CMClass
    Public Shared tastoPremuto As Integer = 0
    ' Public Shared messaggio As List(Of String) = New List(Of String)
    Public Shared sequentialNumber As Integer = 1
    Public Shared localFolder As String = Application.StartupPath
    Public Shared localDBFolder As String = localFolder & "\DB\"
    Public Shared localFWFolder As String = localFolder & "\FW\"
    Public Shared localDLFolder As String = localFolder & "\Download\"
    Public Shared configIni As New INIFile(Application.StartupPath & "\setup.ini")
    Public Shared lingua As String = configIni.ReadValue("option", "language")
    Public Shared connessioneLog As String = "RS232"
    Public Shared serverDBFolder As String = configIni.ReadValue("option", "DatabaseFolder")
    Public Shared serverFWFolder As String = configIni.ReadValue("option", "FirmwareFolder")
    Public Shared databaseName As String = configIni.ReadValue("option", "DatabaseName")
    Public Shared stringaConnessione As String = "provider=Microsoft.Jet.OLEDB.4.0;Data Source =" & localDBFolder & databaseName
    Public Shared objConn As OleDbConnection = New OleDbConnection(stringaConnessione)
    Public Shared Tasto As Integer = 0
    Public Shared Inferiore As _inferiore
    Public Shared Function Messaggio(ByVal indice As Integer) As String
        Dim msg As New INIFile(localFolder & "\" & lingua & ".txt")
        Messaggio = msg.ReadValue("mex", "mex" & indice)
    End Function

    ''' <summary>
    ''' Definizione colori da utilizzare nell'applicazione
    ''' </summary>
    Structure ArcaColor
        'Public Shared sfondo As Color = Color.FromArgb(&HFF55166E)
        'Public Shared errore As Color = Color.FromArgb(&HFFE45620)
        'Public Shared istruzione As Color = Color.FromArgb(&HFF1A3281)
        'Public Shared messaggio As Color = Color.FromArgb(&HFFFFFFFF)
        'Public Shared attesa As Color = Color.FromArgb(&HFF30E410)
        'Public Shared OK As Color = Color.FromArgb(&HFFAEBF37)
        'Public Shared KO As Color = Color.FromArgb(&HFFA00000)

        Public Shared sfondo As Color = Color.FromArgb(&HFF9B9B9B)
        Public Shared errore As Color = Color.FromArgb(&HFFE45620)
        Public Shared istruzione As Color = Color.FromArgb(&HFF1A3281)
        Public Shared messaggio As Color = Color.FromArgb(&HFF000000)
        Public Shared attesa As Color = Color.FromArgb(&HFF30E410)
        Public Shared OK As Color = Color.FromArgb(&HFFAEBF37)
        Public Shared KO As Color = Color.FromArgb(&HFFA00000)
        'Public Shared Warning As Color = Color.FromArgb(&HFFF2F200)
    End Structure

    '''' <summary>
    '''' Caricamento messaggi che verranno visualizzati
    '''' </summary>
    '''' <param name="language">Nome del file della lingua da caricare senza l'estensione</param>
    'Public Shared Sub CaricaMessaggi(ByVal language As String)
    '    Dim iMessaggio As Integer = 0
    '    Dim linguaIni As New INIFile(localFolder & "\" & language & ".ini")
    '    Do
    '        messaggio.Add(linguaIni.ReadValue("messages", "msg" & iMessaggio))
    '        If messaggio(iMessaggio) = "!!END!!" Or messaggio(iMessaggio) = "Failed" Then Exit Do
    '        iMessaggio += 1
    '    Loop
    '    If iMessaggio = 0 Then
    '        MsgBox("LANGUAGE INI FILE PROBLEM.", vbCritical, "CRITICAL ERROR")
    '        End
    '    End If
    'End Sub

    ''' <summary>
    ''' Scrittura del messaggio sull'oggetto indicato
    ''' </summary>
    ''' <param name="testo">Testo da mostrare</param>
    ''' <param name="nomeObj">Nome dell'oggetto sulla quale scrivere il testo</param>
    ''' <param name="colore">Colore del testo da mostrare (default = messaggio)</param>
    Public Shared Sub ScriviMessaggio(ByVal testo As String, ByVal nomeObj As Object, Optional ByVal colore As Color = Nothing)
        If colore = Nothing Then
            colore = ArcaColor.messaggio
        End If
        nomeObj.Text = testo
        nomeObj.ForeColor = colore
    End Sub



    ''' <summary>
    ''' Struttura contenente le parti della macchina
    ''' </summary>
    Public Structure SMacchina
        Dim Codice As String
        Dim IDcliente As Integer
        Dim NomeCliente As String
        Dim Categoria As String
        Dim CategoriaCompleta As String
        Dim PaeseDestinazione As String
        Dim SerialNumber As String
        Dim SerialNumberCompleto As String
        Dim ControllerSerialNumber As String
        Dim ControllerCodice As String
        Dim CRM As String
        Dim CD80 As String
        Dim nCD80 As Integer
        Dim CassetteNumber As Integer
        Dim LettoreModello As String
        Dim LettoreCodice As String
        Dim LettoreMatricola As String
        Dim CD80AMatricola As String
        Dim CD80BMatricola As String
        Dim CD80CMatricola As String
        Dim CD80DMatricola As String
        Dim MacAddress As String
        Dim MacAddressNew As String
        Dim MacAddressOld As String
        Dim HostName As String
        Dim LettoreFW As String
        Dim LettoreCDF As String
        Dim LettoreCurrency As String
        Dim SuiteCodice As String
        Dim SuiteNome As String
        Dim Skin As String
        Dim Language As String
        Dim DisplayMessage As String
        Dim DisplayVolume As String
        Dim DisplayLingua As String
        Dim DisplaySfondo As String
        Dim IDunitConfiguration As String
        Dim CustomizationCodice As String
        Dim ShiftCenterValue As String
        Dim BagFW As String
        Dim ScFW As String
        Dim EmfuFW As String
        'Dim ExtendedStatus As SExtendedStatus
        Dim Test As STest
    End Structure
    'Public Shared Macchina As SMacchina = New SMacchina

    Structure _inferiore
        Dim categoria As String
        Dim Matricola_Inferiore As String
        Dim Codice_cliente As Integer
        Dim Nome_cliente As String
        Dim Codice_Inferiore As String
        Dim Codice_Macchina As String
        Dim Matricola_Controller As String
        Dim Matricola_lettore As String
        Dim Codice_FW_Suite As String
        Dim Codice_FW_castomization As String
        Dim MatricolaCassA As String
        Dim MatricolaCassB As String
        Dim CD80_Matricola_A As String
        Dim CD80_Matricola_B As String
        Dim CD80_Matricola_C As String
        Dim CD80_Matricola_D As String
        Dim Data As String
        Dim Ora As String
        Dim CRM As String
        Dim CD80 As Integer
        Dim assign As Boolean
        Dim Cassette_number As Boolean
        Dim test_cd80 As Boolean
        Dim setup_cassette As Boolean
        Dim Deposito As Boolean
        'Dim Codice_FW_lettore As String
        'Dim Codice_Fw_riferimenti As String
        Dim Test As _test
    End Structure

    Structure _test
        Dim test As Boolean
        Dim photo_sensor As Boolean
        Dim cassette_photo() As _cassette_photo
        Dim Bn_riconosciute As Integer
        Dim Bn_rifiutate As Integer
        Dim RiduzioneCap As Boolean
        Dim FwCD80 As String

    End Structure

    Structure _cassette_photo
        Dim Limiti As limiti_cassette
        Dim Foto_in As Byte
        Dim Foto_Out As Byte
    End Structure

    Structure limiti_cassette
        Dim F_in_min As Integer
        Dim F_in_max As Integer
        Dim F_out_min As Integer
        Dim F_out_max As Integer
        Dim F_In_Box_min As Integer
        Dim F_In_Box_max As Integer
    End Structure

    ''' <summary>
    ''' Struttura dei FW standard
    ''' </summary>
    Public Structure SFirmware
        Dim Ver As String
        Dim Rel As String
    End Structure

    ''' <summary>
    ''' Struttura FW Bag
    ''' </summary>
    Public Structure SFwBag
        Dim MBAG As SFirmware
        Dim EMFU As SFirmware
    End Structure

    ''' <summary>
    ''' Struttura dei singoli fotosensori standand
    ''' </summary>
    Public Structure SPhoto
        Dim Valore As Integer
        Dim Min As Integer
        Dim Max As Integer
    End Structure

    ''' <summary>
    ''' Struttura dei fotosensori Bag
    ''' </summary>
    Public Structure SPhotoBag
        Dim Transport As SPhoto
        Dim NotesInLift As SPhoto
        Dim Ir1Empty As SPhoto
        Dim Ir2Empty As SPhoto
        Dim Ir1NotEmpty As SPhoto
        Dim Ir2NotEmpty As SPhoto
    End Structure

    ''' <summary>
    ''' Struttura dei fotosensori del trasporto superiore
    ''' </summary>
    Public Structure SPhotoAll
        Dim InCenter As SPhoto
        Dim InLeft As SPhoto
        Dim HMax As SPhoto
        Dim Feed As SPhoto
        Dim C1 As SPhoto
        Dim Shift As SPhoto
        Dim InQ As SPhoto
        Dim Count As SPhoto
        Dim Out As SPhoto
        Dim C3 As SPhoto
        Dim C4A As SPhoto
        Dim C4B As SPhoto
        Dim Rej As SPhoto
        Dim Fpbksus As SPhoto
        Dim InBox As SPhoto
    End Structure

    ''' <summary>
    ''' Struttura dei fotosensori del Safe
    ''' </summary>
    Public Structure SPhotoSafe
        Dim CashLeft As SPhoto
        Dim CashRight As SPhoto
        Dim Thick As SPhoto
        Dim FinRcyc As SPhoto
        Dim EndV As SPhoto
        Dim InCD80A As SPhoto
        Dim InCD80B As SPhoto
        Dim InCD80C As SPhoto
        Dim InCD80D As SPhoto
    End Structure

    ''' <summary>
    ''' Struttura della taratura di tutti i fotosensori
    ''' </summary>
    Public Structure STaratura
        Dim FotoBag As SPhotoBag
        Dim Foto As SPhotoAll
        Dim FotoSafe As SPhotoSafe
    End Structure
    Public Shared Taratura As STaratura = New STaratura

    ''' <summary>
    ''' Struttura del singolo cassetto
    ''' </summary>
    Public Structure SCassette
        Dim noteId As String
        Dim bnNumber As Integer
        Dim freeCap As Integer
        Dim name As String
        Dim enable As String
        Dim firmware As String
    End Structure


    ''' <summary>
    ''' Struttura standard dei test
    ''' </summary>
    Public Structure STest
        Dim DataInizio As String
        Dim DataFine As String
        Dim OraInizio As String
        Dim OraFine As String
        Dim ToolVersion As String
        Dim TestLocation As String
        Dim TestUser As String
        Dim Categoria As String
        Dim EsitoFinale As Boolean
        Dim Fase As String
        Dim DateTime As Boolean
        Dim Assign As Boolean
        Dim UnitConfiguration As Boolean
        Dim CassetteSetup As Boolean
        Dim CassetteNumber As Integer
        Dim Lan As Boolean
        Dim CancellazioneBanchi As Boolean
        Dim ClientCDFDownload As Boolean
        Dim LettoreCDF As String
        Dim ReaderFWDownload As Boolean
        Dim PhotoAdjust As Boolean
        Dim CustomDownload As Boolean
        Dim SuiteDownload As Boolean
        Dim CRM As Boolean
        Dim Bag As Boolean
        Dim MatricolaMacchina As String
        Dim MatricolaController As String
        Dim MatricolaLettore As Boolean
        Dim TestConnessione As Boolean
        Dim TipoConnessione As String
        Dim TaraturaFotoBag As Boolean
        Dim TaraturaFoto As Boolean
        Dim MiniDeposito As Boolean
        Dim Seriali As Boolean
        Dim USB As Boolean
        Dim ImpostazioneTipo As Boolean
        Dim Alarm As Boolean
        Dim UMA As Boolean
        Dim Suite As String
        Dim Custom As String
        Dim SerialNumberCompleto As String
        Dim ControllerSerialNumber As String
        Dim LettoreCodice As String
        Dim LettoreMatricola As String
        Dim MacAddressNew As String
        Dim MacAddressOld As String
        Dim LettoreCurrency As String
        Dim SuiteCodice As String
        Dim Skin As String
        Dim Language As String
        Dim DisplayMessage As String
        Dim DisplayVolume As String
        Dim DisplayLingua As String
        Dim DisplaySfondo As String
        Dim CustomizationCodice As String
        'Dim ShiftCenterValue As Integer
        'Dim HostName As String
        'Dim MacAddressNew As String
        'Dim MacAddressOld As String
        'Dim ReaderFW As String
        'Dim CustomCode As String
        'Dim SuiteCode As String
        'Dim CDFLettore As String
    End Structure
    'Public Shared LogTest As STest = New STest

    Public Structure SLog
        Dim Anagrafica As SMacchina
    End Structure

    Public Structure SCashData
        Dim A As SCassette
        Dim B As SCassette
        Dim C As SCassette
        Dim D As SCassette
        Dim E As SCassette
        Dim F As SCassette
        Dim G As SCassette
        Dim H As SCassette
        Dim I As SCassette
        Dim J As SCassette
        Dim K As SCassette
        Dim L As SCassette
        Dim escrow As SBag
        Dim bag As SBag
        Dim totCassetti As Integer
    End Structure
    Public CashData As SCashData

    Public Structure SBag
        Dim bnNumber As Integer
        Dim freeCap As Integer
        Dim name As String
        Dim status As String
    End Structure

    Public Shared Sub Aspetta(Optional ByVal mSec As Integer = 1000)
        Application.DoEvents()
        Threading.Thread.Sleep(mSec)
    End Sub

    ''' <summary>
    ''' Ciclo di attesa pressione tasto(RISPOSTA : INVIO,R,ESC,OK=tastoSpecifico)
    ''' </summary>
    ''' <param name="tastoSpecifico">Codice asci decimale del tasto da attendere</param>
    ''' <returns></returns>
    Public Shared Function AttendiTasto(Optional ByVal tastoSpecifico As String = "", Optional ByVal oggetto As Object = Nothing) As String
        'Dim tasto As Integer
        If oggetto IsNot Nothing Then
            oggetto.focus()
        End If
        AttendiTasto = ""
        tastoPremuto = 0
        Do Until Len(AttendiTasto) > 0
            If Len(tastoSpecifico) > 0 Then
                Select Case tastoSpecifico
                    Case "INVIO", "invio", "ENTER", "enter"
                        Tasto = 13
                    Case "ESC", "esc"
                        Tasto = 27
                    Case Else
                        Tasto = Asc(UCase(tastoSpecifico))
                End Select
                If tastoPremuto = Tasto Then
                    AttendiTasto = "OK"
                End If
            Else
                Select Case tastoPremuto
                    Case 0
                    Case 13
                        AttendiTasto = "INVIO"
                    Case 82, 114
                        AttendiTasto = "R"
                    Case 27
                        AttendiTasto = "ESC"
                    Case Else
                        AttendiTasto = "OK"
                End Select
                'End If
            End If

            Application.DoEvents()
        Loop
    End Function

    ''' <summary>
    ''' Scrive il singolo dato del log
    ''' </summary>
    ''' <param name="nomeFile"></param>
    ''' <param name="sezione"></param>
    ''' <param name="valore"></param>
    Public Shared Sub ScriviLog(ByVal nomeFile As String, ByVal sezione As String, ByVal valore As String)
        Using fileLog As StreamWriter = File.AppendText(localFolder & "\" & nomeFile)

        End Using
    End Sub

End Class

﻿Public Class Grid
    Private _Rows As Integer = 1
    Private _Cols As Integer = 1
    Public buf(,) As String
    Public buf_color(,) As Color
    Public buf_back_color(,) As Color
    Private _Left As Integer = 200
    Private _Top As Integer = 150

    Public Property Left() As Integer
        Get
            Return _Left
        End Get
        Set(ByVal value As Integer)
            If value > 0 Then
                _Left = value

            End If

        End Set
    End Property
    Public Property Top() As Integer
        Get
            Return _Top
        End Get
        Set(ByVal value As Integer)
            If value > 0 Then
                _Top = value

            End If

        End Set
    End Property
    Public Property Rows() As Integer
        Get
            Return _Rows
        End Get
        Set(ByVal value As Integer)
            If value > 0 Then
                If _Rows < value Then
                    _Rows = value
                    If Cols > 0 Then

                        ReDim Preserve buf(Cols, Rows)
                        ReDim Preserve buf_color(Cols, Rows)
                        ReDim buf_back_color(Cols, Rows)
                    End If
                Else
                    _Rows = value
                    ReDim buf(Cols, Rows)
                    ReDim buf_color(Cols, Rows)
                    ReDim buf_back_color(Cols, Rows)
                End If
            End If
        End Set
    End Property

    Public Property Cols() As Integer
        Get
            Return _Cols
        End Get
        Set(ByVal value As Integer)

            If value > 0 Then
                If _Cols <> value Then
                    _Cols = value
                    ReDim buf(Cols, Rows)
                    ReDim buf_color(Cols, Rows)
                    ReDim buf_back_color(Cols, Rows)
                End If
            End If

        End Set
    End Property

    Public Sub Riempi_Griglia(ByVal Griglia As Object)
        Try
            Griglia.ColumnCount = _Cols
            Griglia.RowCount = _Rows
            Cancella_Griglia(Griglia)
            For a = 0 To _Cols - 1
                For b = 0 To _Rows - 1
                    Griglia.item(a, b).value = buf(a, b)
                    Griglia.Item(a, b).Style.ForeColor = buf_color(a, b)
                    Griglia.item(a, b).Style.BackColor = buf_back_color(a, b)

                Next
            Next
            Griglia.ClearSelection()
            'Ridimensiona_Griglia(Griglia)
        Catch Ex As Exception
        End Try
    End Sub
    Public Sub Ridimensiona_Griglia(ByVal Griglia As Object, Optional ByVal Riposiziona As Boolean = True)

        Dim Larghezza As Integer
        Dim Altezza As Integer

        For a = 1 To Griglia.Columns.Count
            Griglia.AutoResizeColumn(a - 1)
        Next

        Altezza = 3
        For a = 1 To Griglia.Rows.Count
            If Altezza > Screen.PrimaryScreen.Bounds.Height - 55 - _Top Then
                Griglia.ScrollBars = ScrollBars.Vertical
                Griglia.enabled = True
            Else
                frmPrincipale.Grid1.ScrollBars = ScrollBars.None
                Altezza = Altezza + Griglia.Rows(a - 1).Height
            End If
        Next
        Griglia.Height = Altezza
        If Griglia.ScrollBars = ScrollBars.Vertical Then
            Larghezza = 20
        Else
            Larghezza = 3
        End If

        For a = 1 To Griglia.Columns.Count
            Larghezza = Larghezza + Griglia.Columns(a - 1).Width
        Next
        Griglia.Width = Larghezza

        If Riposiziona = True Then
            Call Posizione(Griglia, True)
        End If

        Griglia.visible = True

        Application.DoEvents()
    End Sub
    Public Sub Posizione(ByVal griglia As Object, ByVal centra As Boolean)

        If centra = False Then
            griglia.left = _Left
            griglia.top = _Top
        Else
            griglia.top = _Top
            griglia.left = (Screen.PrimaryScreen.Bounds.Width - griglia.width) / 2
        End If

    End Sub
    Public Sub font_griglia(ByVal Griglia As Object, ByVal Nome_Font As String, ByVal Size As Byte, ByVal Style As FontStyle)

        Griglia.Font = New Font(Nome_Font, Size, FontStyle.Bold)
        'Call Ridimensiona_Griglia(Griglia)

    End Sub
    Public Sub Cancella_Griglia(ByVal Griglia As Object)
        Try
            If Not IsNothing(Griglia) Then
                For a = 0 To Rows - 1
                    For b = 0 To Cols - 1
                        Griglia.Item(b, a).Style.ForeColor = Color.Black
                        Griglia.Item(b, a).Value = ""
                    Next
                Next
                Griglia.visible = False
            End If
        Catch Ex As Exception
        End Try
    End Sub

    Public Sub Colonna_Verde(ByVal Col As Integer)
        For a = 0 To Rows - 1
            buf_color(Col, a) = CMClass.ArcaColor.OK
        Next

    End Sub
    Public Sub Colonna_Nera(ByVal Col As Integer)
        For a = 0 To Rows - 1
            buf_color(Col, a) = CMClass.ArcaColor.messaggio
        Next

    End Sub
    Public Sub Riga_Verde(ByVal Row As Integer)
        For a = 0 To Cols - 1
            buf_color(a, Row) = CMClass.ArcaColor.OK
        Next
    End Sub

    Public Sub Riga_Nera(ByVal Row As Integer)
        For a = 0 To Cols - 1
            buf_color(a, Row) = CMClass.ArcaColor.messaggio
        Next
    End Sub
End Class



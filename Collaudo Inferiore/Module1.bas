Attribute VB_Name = "Sub_Comuni"
Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Public Declare Function CopyFile Lib "kernel32" Alias "CopyFileA" (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal bFailIfExists As Long) As Long
'Public Declare Function GetFileVersionInfo Lib "kernel32" Alias "GetFileVersionInfoA" (ByVal lptstrFilename As String, ByVal dwHandle As Long, ByVal dwLen As Long, lpData As Any) As Long
'Public Declare Function GetFileVersionInfoSize Lib "kernel32" Alias "GetFileVersionInfoSizeA" (ByVal lptstrFilename As String, lpdwHandle As Long) As Long

Public comandoSingoloRisposta(300) As String

Const SYNCHRONIZE = &H100000    ' Wait forever
Const INFINITE = &HFFFF         ' The state of the specified object is signaled
Const WAIT_OBJECT_0 = 0         ' The time-out interval elapsed & the object�s state is not signaled

Const WAIT_TIMEOUT = &H102

Type livellobasso
    aaa As String
End Type

Type livellomedio
    bbb As livellobasso
End Type

Type livelloalto
    ccc As livellomedio
End Type

Public livello As livelloalto

Type BN_CAT
    name As String
    Number As Byte
End Type

Type Sta
    Host As String
    Dsp As String
End Type

Public status As Sta
    
Type limBag
    fTransportMin As Byte
    fTransportMax As Byte
    fNotesInLiftMin As Byte
    fNotesInLiftMax As Byte
    fIr1EmptyCalib_min As Byte
    fIr1EmptyCalib_max As Byte
    fIr2EmptyCalib_min As Byte
    fIr2EmptyCalib_max As Byte
    fIr1NotEmptyCalib_min As Byte
    fIr1NotEmptyCalib_max As Byte
    fIr2NotEmptyCalib_min As Byte
    fIr2NotEmptyCalib_max As Byte
End Type

Type bag
    limiti As limBag
    fTransport As Integer
    fNotesInLift As Integer
    fEmptyIr1 As Integer
    fEmptyIr2 As Integer
    fNotEmptyIr1 As Integer
    fNotEmptyIr2 As Integer
    fullSensorCalib As Boolean
End Type

Type lim_safe
    F_Cash_L_min As Byte
    F_Cash_L_max As Byte
    F_Cash_R_min As Byte
    F_Cash_R_max As Byte
    F_Endv_min As Byte
    F_Endv_max As Byte
    F_InBag_min As Byte
    F_InBag_max As Byte
    F_Aux1_min As Byte
    F_Aux1_max As Byte
    F_in_cd_80_min As Byte
    F_in_cd_80_max As Byte
    F_Thick_min As Byte
    F_Thick_max As Byte
    F_InRcyc_min As Byte
    F_InRcyc_max As Byte
End Type

Type saf
    limiti As lim_safe
    F_Cash_L As Byte
    F_Cash_R As Byte
    F_Endv As Byte
    F_InBag As Byte
    F_Aux1 As Byte
    F_in_cd_80_A As Byte
    F_in_cd_80_B As Byte
    F_in_cd_80_C As Byte
    F_in_cd_80_D As Byte
    F_Thick As Byte
    F_Finrcyc As Byte
End Type

Type lim_rea
    F_Shift_min  As Byte
    F_Shift_max As Byte
    F_Inq_min As Byte
    F_Inq_max As Byte
    F_Count_min As Byte
    F_Count_max As Byte
    F_Out4A_min As Byte
    F_Out4A_max As Byte
    F_Out4B_min As Byte
    F_Out4B_max As Byte
    F_Out3_min As Byte
    F_Out3_max As Byte
    F_Reject_min As Byte
    F_Reject_max As Byte
    F_In_min As Byte
    F_In_max As Byte
    F_Feeder_min As Byte
    F_Feeder_max As Byte
    F_Feeder2_min As Byte
    F_Feeder2_max As Byte
    F_Inl_min As Byte
    F_Inl_max As Byte
    F_Out_min As Byte
    F_Out_max As Byte
    F_Hmax_min As Byte
    F_Hmax_max As Byte
    F_In_Box_min As Byte
    F_In_Box_max As Byte

End Type

Type rea
    limiti As lim_rea
    F_Shift As Byte
    F_Inq As Byte
    F_Count As Byte
    F_Out4A As Byte
    F_Out4B As Byte
    F_Out3 As Byte
    F_Reject As Byte
    F_In As Byte
    F_Feeder As Byte
    F_Feeder2 As Byte
    F_Inl As Byte
    F_Out As Byte
    F_Hmax As Byte
    F_In_Box As Byte

End Type

Type StatusHC
    HC As String
    Tow1 As String
    Tow2 As String
    Tow3 As String
    Tow4 As String
    CasA As String
    CasB As String
    CasC As String
    CasD As String
    CasE As String
    CasF As String
    CasG As String
    CasH As String
    IdModule As String
    IdError As Integer
    Module As String
    Error As String
End Type

Type SPhoto
    name As String
    adjValue As Integer
    adjMin As Integer
    adjMax As Integer
End Type

Type PhotoHC
    phIn As SPhoto
    phEmpty As SPhoto
    phFull As SPhoto
    phFill As SPhoto
    phSafeInA As SPhoto
    phSafeInB As SPhoto
    phSafeTr1 As SPhoto
    phSafeTr2 As SPhoto
    phTowTr1 As SPhoto
End Type

Type SSorter
    name As String
    OpenValue As Integer
    OpenMin As Integer
    OpenMax As Integer
    CloseValue As Integer
    CloseMin As Integer
    CloseMax As Integer
End Type

Type SorterHC
    CassetteA As SSorter
    CassetteC As SSorter
    CassetteE As SSorter
    CassetteG As SSorter
    Tower0 As SSorter
    Tower1 As SSorter
    Tower2 As SSorter
    Tower3 As SSorter
    Safe As SSorter
End Type

Type HcDetails
    status As StatusHC
    Photos As PhotoHC
    Sorter As SorterHC
End Type


Type Mac
    Codice As String
    Categoria As String
    CRM As String
    cd80 As Byte '0 = assente, 1 = un vassoio (2box), 2 = due vassoi (4box)
    Cliente As String
    Nome_Cliente As String
    Lettore As String
    Real_time As rea
    Safe As saf
    FotoBag As bag
    Matricola_Lettore As String
    Codice_lettore As String
    Matricola_Macchina As String
    Matricola_Macchina_Completa As String
    Matricola_Controller As String
    CD80_Matricola_A As String
    CD80_Matricola_B As String
    CD80_Matricola_C As String
    CD80_Matricola_D As String
    Mac_Address_old As String
    Mac_Address_new As String
    Data_ora As Boolean
    Assign As Boolean
    Unit_configuration As Boolean
    Cassette_Number As Boolean
    Setup_Cassetti As Boolean
    Cancellazione_Banchi As Boolean
    FW_Lettore As String
    Download_FW_Lettore As Boolean
    Download_customization As Boolean
    CDF_Lettore As String
    download_suite As Boolean
    Download_CDF_Cliente As Boolean
    Test_Lan As Boolean
    Taratura_Foto As Boolean
    Mini_Deposito As Boolean
    Test_CRM As Boolean
    Test_seriali As Boolean
    Test_USB As Boolean
    Impostazione_Tipo As Boolean
    Cod_suite As String
    Cod_customization As String
    Cod_controller As String
    Cod_cassetti As String
    SHIFT_CENTER_VALUE As String
    Numero_cassetti_in_test As Byte
    TaraturaFotoBag As Boolean
    fotoInCassAValue As String
    fotoOutCassAValue As String
    fotoInCassBValue As String
    fotoOutCassBValue As String
    fotoInCassCValue As String
    fotoOutCassCValue As String
    fotoInCassDValue As String
    fotoOutCassDValue As String
    fotoInCassEValue As String
    fotoOutCassEValue As String
    fotoInCassFValue As String
    fotoOutCassFValue As String
    fotoInCassGValue As String
    fotoOutCassGValue As String
    fotoInCassHValue As String
    fotoOutCassHValue As String
    fotoInCassIValue As String
    fotoOutCassIValue As String
    fotoInCassJValue As String
    fotoOutCassJValue As String
    fotoInCassKValue As String
    fotoOutCassKValue As String
    fotoInCassLValue As String
    fotoOutCassLValue As String
    fwMBAG As String
    fwSC As String
    fwEMFU As String
    testSwDoor As Boolean
    testSwCage As Boolean
    testSwGate As Boolean
    testSwBag As Boolean
    testSwRcyc As Boolean
    testMivIn As Boolean
    testMivOut As Boolean
    testMovBotola As Boolean
    HcModule As HcDetails
End Type

Type Banc_cas
    nome As String
    N_note As Integer
    denomination As String
End Type

Type Cash_d
    nome As String
    N_note As Integer
    denomination As String
    Enabled As String
    Capacity As Integer
End Type


Type dep
    rc As String
    Banconote_tot As Byte
    Banconote_Rif As Byte
    Banconote_UNFit As Byte
    Bn_cas(12) As Banc_cas
End Type

Type PREL
    rc As String
    Bn_cas(12) As Banc_cas
End Type

Type CasH
    rc As String
    Bn_cas(12) As Cash_d
End Type

Public Cash_dat As CasH
Public Deposito As dep
Public prelievo As dep

Public nbanchi_attivi As String
Public Macchina As Mac
Public direct As Boolean
Public Stringa_di_connessione As String
Public nomeFile As String
Public nomeSPT  As String
Public Matricola_Modulo As String
Public Tipo_Modulo As String
Public OKCOMM As Boolean
Public TRASPRIC As String
Public Scala As Single
Public Dl0 As Boolean
Public Rx$
Public Suspend As Boolean
Public Risptra As String
Public WorkDir As String
Public Tasto As Integer
Public Ora_inizio As String
Public cm As Integer
Public Salva_Tutto As String
Public aaak As String
Public server As String
Public Com_CM As Integer
Public Com_Lettore As Integer
Public Com As Integer
Public salva_com As Boolean
Public Ricez As String
Public Const safe_id_485_comunication = "1"
Public Const real_time_id_485_comunication = "2"
Public Const reader_id_485_comunication = "3"
Public Transparent_ON As Boolean
Public Lato As String
Public Disposizione As Boolean
Public Entra_in_Transparent As Boolean
Public Host_name As String
Public Ip_Macchina As String
Public Old_Ip As String
Public Versione_Software As String
Public Stazione_Collaudo As String
Public Mex() As String
Public Zip_Path As String
Public DatabaseName As String
Public Lan_data As Boolean
Public Cat_2 As BN_CAT
Public Cat_3 As BN_CAT
Public Lettura_FA_iniziale As String
Public Registra_Flag_avanzamento As Boolean
Public Stop_polling_suite As Boolean

'Public Function GetFileVersionData(Fname As String) As String
'    Dim sInfo As String
'    Dim retValue As Long
'    Dim lSizeOf As Long
'    Dim n As Integer
'    On Error GoTo errore
'    lSizeOf = GetFileVersionInfoSize(Fname, 0)
'    If lSizeOf > 0 Then
'        sInfo = String$(lSizeOf, 0)
'        retValue = GetFileVersionInfo(Fname, 0&, lSizeOf, sInfo)
'        If retValue Then
'            n = InStr(sInfo, "FileVersion")
'            If n > 0 Then
'                n = n + 12
'                GetFileVersionData = Mid$(sInfo, n)
'            End If
'        End If
'    End If
'    Exit Function
'
'errore:
'    MsgBox err.Description, 16, "GetFileVersionData"
'    Resume Next
'End Function


Sub Aggiorna_Stato()
    
    Dim frm_ptr As Form
    Dim n As Integer
    
    Set frm_ptr = Form1
    
    
    Call Traspsend("F3", 1, 0.1)
    stat = (TRASPRIC)
    If Len(stat) = 0 Then stat = "00"
    If Len(stat) = 1 Then stat = "0" & stat
    If Len(stat) > 2 Then stat = Left(stat, 2)
    frm_ptr.Label31(0).Caption = Mex(114) & "  " & stat 'Host
    status.Host = stat
    
    Call Traspsend("D338", 1, 0.1)
    stat = (TRASPRIC)
    If Len(stat) = 0 Then stat = "00"
    If Len(stat) = 1 Then stat = "0" & stat
    If Len(stat) > 2 Then stat = Left(stat, 2)
    frm_ptr.Label31(1).Caption = Mex(75) & "  " & stat 'Dsp
    status.Dsp = stat
    
    Call Traspsend("D3320000030F", 4, 0.1)
    stat = Right(Int_Mag_w(TRASPRIC), 2)
    If Len(stat) = 0 Then stat = "00"
    If Len(stat) = 1 Then stat = "0" & stat
    frm_ptr.Label31(2).Caption = Mex(193) & "  " & stat 'Mag
    
    Call Traspsend("D37C54", 2, 0.1)
    stat = TOGLIE_0((TRASPRIC))
    If Len(stat) = 0 Then stat = "00"
    If Len(stat) = 1 Then stat = "0" & stat
    frm_ptr.Label31(3).Caption = Mex(272) & "  " & stat 'Tape
    
    Set frm_ptr = Nothing

End Sub

Function Int_Mag_w(stri As String)
    Int_Mag_w = ""
    For a = 1 To Len(stri) Step 4
        Int_Mag_w = Int_Mag_w + Mid$(TOGLIE_0(stringa_ricezione(stri)), a, 4)
    Next

End Function


Function Controllo_Matricola_Lettore(M_L As String) As Boolean
    
    Controllo_Matricola_Lettore = True
    
    If Mid$(M_L, 1, 5) = Macchina.Codice_lettore Then GoTo matr
    
    
    If Left(M_L, 2) <> "RS" Then
        Controllo_Matricola_Lettore = False
    End If
    
    For a = 3 To 4
        If Mid$(M_L, a, 1) < "0" Or Mid$(M_L, a, 1) > "9" Then
            Controllo_Matricola_Lettore = False
        End If
    Next
    
    For a = 5 To 5
        If Mid$(M_L, a, 1) < "A" Or Mid$(M_L, a, 1) > "Z" Then
            Controllo_Matricola_Lettore = False
        End If
    Next
    
matr:
    
    For a = 6 To 12
        If Mid$(M_L, a, 1) < "0" Or Mid$(M_L, a, 1) > "9" Then
            Controllo_Matricola_Lettore = False
        End If
    Next

End Function

Sub att_tasto(continua As String)
    Tasto = 0
    If continua = "" Then
        Tasto = 0
        Do While Not Tasto <> 0
            DoEvents
        Loop
    End If
    If Len(continua) = 1 Then
        Tasto = 0
        Do While Not Tasto = Asc(UCase(continua)) Or Asc(LCase(continua))
            DoEvents
        Loop
    End If
    Form1.Show
End Sub
Sub Attendi_Tasto()
    riapri_com = False
    If Hcom > 0 Then
        If direct = False Then
            If Transparent_ON = False Then Traspin: trasp = True
            Call Traspsend("F3", 2, 0.5)
            If Len(TRASPRIC) > 0 Then Aggiorna_Flag_Avanzamento
            If trasp = True Then Traspout
            Close_com
            riapri_com = True
        End If
    End If
    
    Tasto = 0
    Do While Not Tasto <> 0
        DoEvents
        'Form1.Show
    Loop
    
    If riapri_com = True Then Open_com
    Form1.Show
End Sub

Function Attendi_Tasto_loop_stato() As Boolean
    
    Attendi_Tasto_loop_stato = True
    If direct = False Then Call Aggiorna_Stato
    If Form1.Frame17.Visible = False Then Call disponi_frame_stato
    Tasto = 0
    
    Call Traspsend("F3", 1, 2)
    Call Aggiorna_Flag_Avanzamento
    Do While Not Tasto <> 0
        msDELAY (0.3)
    
        Call Traspsend("F3", 1, 2)
        
        If Len(TRASPRIC) = 0 Then
            msDELAY (1)
            Call Traspsend("F3", 1, 2)
            'If Len(TRASPRIC) = 0 Then
            If Len(TRASPRIC) = 0 Then
                msDELAY (1)
                Call Traspsend("F3", 1, 2)
                Form1.Label1.Caption = Mex(171) 'Interrotta la comunicazione con il lettore in fase di attesa tasto, la prova deve riprendere dall'inizio, premere un tasto
                Attendi_Tasto
                Attendi_Tasto_loop_stato = False
                Form1.Show
                Exit Function
            End If
            'End If
        End If
        DoEvents
        'Form1.Show
    Loop
    Form1.Show
End Function

Sub disponi_frame_stato()

    Form1.Frame17.Top = Form1.Height - Form1.Frame17.Height - 800
    Form1.Frame17.Left = 200 * Scala
    Form1.Frame17.Visible = True

End Sub
Sub MotorOff(tipo_macchina As String)
    
    Select Case tipo_macchina
    
        Case "CM20", "CM18", "CM18T", "CM18EVOT", "CM20S", "CM20T", "CM18B", "CM18EVO", "CM18SOLO", "CM18SOLOT", "OM61", "CM18HC"
        
            Call Traspsend("D212", 0, 0)
            Call Traspsend("D203", 0, 0)
            
        Case "CM24", "CM14", "CM24_5BNS", "CM14HC", "CM24_X_BNS"
        
            
            Call Traspsend("D207", 0, 0)
            Call msDELAY(0.3)
            Call Traspsend("D234", 0, 0)
            Call msDELAY(0.3)
            Call Traspsend("D21B", 0, 0)
            Call msDELAY(0.3)
            Call Traspsend("D203", 0, 0)
    
    End Select

End Sub

Sub MotorOn(tipo_macchina As String)
        
    Select Case tipo_macchina
        
        Case "CM20", "CM18", "CM18T", "CM18EVOT", "CM20S", "CM20T", "CM18B", "CM18EVO", "CM18SOLO", "CM18SOLOT", "OM61", "CM18HC"
            Call Traspsend("D201", 1, 2)
    
        Case "CM24", "CM14", "CM24_5BNS", "CM14HC", "CM24_X_BNS"
        
            If Lato = "L" Then
                 Call Traspsend("D205", 0, 0)
            Else
                Call Traspsend("D204", 0, 0)
            End If
            Call msDELAY(0.3)
            Call Traspsend("D201", 0, 0)
            Call msDELAY(1)
            Call Traspsend("D219", 0, 0)
            Call msDELAY(0.3)
            Call Traspsend("D21B", 0, 0)
            Call msDELAY(0.3)
            
    End Select

End Sub

Sub Pre_Sfogliatura(tipo_macchina As String)
    
    Select Case tipo_macchina
    
        Case "CM20", "CM18", "CM18T", "CM18EVOT", "CM20S", "CM20T", "CM18B", "CM18EVO", "CM18SOLO", "CM18SOLOT", "OM61", "CM18HC"
    
    
            Call Traspsend("920F30303031", 0, 0)
            Call msDELAY(0.1)
           
            Call Traspsend("D216", 0, 0)
            Call msDELAY(0.5)
            
            Call Traspsend("D20B", 0, 0)
            Call msDELAY(1)
            
            Call Traspsend("D201", 1, 2)
    
            Call Traspsend("D21C", 0, 0)
            Call msDELAY(0.2)
    
            Call Traspsend("D20D", 0, 0)
            Call msDELAY(1)
        Case "CM24", "CM14", "CM24_5BNS", "CM14HC", "CM24_X_BNS"
            Call MotorOn(tipo_macchina)
            
    End Select

End Sub
Sub Sfogliatura(tipo_macchina As String)
    Select Case tipo_macchina
        Case "CM20", "CM18", "CM18T", "CM18EVOT", "CM20S", "CM20T", "CM18B", "CM18EVO", "CM18SOLO", "CM18SOLOT", "OM61", "CM18HC"
            Call msDELAY(0.2)
            Call Traspsend("D239", 3, 15)
        Case "CM24", "CM14", "CM24_5BNS", "CM14HC", "CM24_X_BNS"
            Call Traspsend("810001", 0, 0)
    End Select

End Sub

Function Attendi_Bn_Sfo(tipo_macchina As String) As Boolean
    Select Case tipo_macchina
        Case "CM20", "CM18", "CM18T", "CM18EVOT", "CM20S", "CM20T", "CM18B", "CM18EVO", "CM18SOLO", "CM18SOLOT", "OM61", "CM18HC"
            'Call ricezione(6, 4)
            If Len(TRASPRIC) <> 6 Then
                Attendi_Bn_Sfo = False
                Exit Function
            Else
                Attendi_Bn_Sfo = True
            End If
        Case "CM24", "CM14", "CM24_5BNS", "CM14HC", "CM24_X_BNS"
            Call Traspsend("A3", 6, 3)
            oldn = Val("&h" + Mid$(TRASPRIC, 3, 2))
            START = Timer
            Do While Not nnote = oldn + 1
                Call Traspsend("A3", 6, 3)
                pippo = pippo + TRASPRIC
                nnote = Val("&h" + Mid$(TRASPRIC, 3, 2))
                If Timer > START + 5 Then
                    Attendi_Bn_Sfo = False
                    Exit Function
                End If
            Loop
            Attendi_Bn_Sfo = True
    End Select
End Function

Sub Post_Sfogliatura(tipo_macchina As String)
    
    Select Case tipo_macchina
    
        Case "CM20", "CM18", "CM18T", "CM18EVOT", "CM20S", "CM20T", "CM18B", "CM18EVO", "CM18SOLO", "CM18SOLOT", "OM61", "CM18HC"
    
            Call msDELAY(1)
            'Call traspsend("D23A", 0, 0)
            'Call msDELAY(0.1)
            'Call traspsend("D21D", 0, 0)
            'Call msDELAY(0.1)
            'Call traspsend("D217", 0, 0)
            'Call msDELAY(0.1)
            
            
    End Select
End Sub

Sub Fine_Sfogliatura(tipo_macchina As String)
    
    Select Case tipo_macchina
    
        Case "CM20", "CM18", "CM18T", "CM18EVOT", "CM20S", "CM20T", "CM18B", "CM18EVO", "CM18SOLO", "CM18SOLOT", "OM61", "CM18HC"
    
            Call Traspsend("D23A", 0, 0)
            Call msDELAY(0.5)
            Call Traspsend("D20B", 0, 0)
            Call msDELAY(0.5)
            Call Traspsend("D21D", 0, 0)
            Call msDELAY(0.5)
            Call Traspsend("D203", 0, 0)
            Call msDELAY(0.5)
            Call Traspsend("D217", 0, 0)
            Call msDELAY(0.5)
    Case Else
        Call msDELAY(1.5)
        Call MotorOff(Macchina.Categoria)
        Call msDELAY(0.5)
        
    End Select

End Sub
Sub Sfoglia_tutte_le_banconote(tipo_macchina As String)
    
    Select Case tipo_macchina
        Case "CM18", "CM20", "CM18T", "CM18EVOT", "CM20S", "CM20T", "CM18B", "CM18EVO", "CM18SOLO", "CM18SOLOT", "OM61", "CM18HC"
            Call Traspsend("920F30323030", 0, 0)
            Call Traspsend("D216", 0, 0)
            Call msDELAY(0.5)
            Call Traspsend("D21C", 0, 0)
            Call msDELAY(1)
            Call Traspsend("D201", 1, 2)
    
            Call Traspsend("D20D", 0, 0)
            Call msDELAY(1)
            Call Traspsend("D239", 0, 0)
            Call msDELAY(1)
    
        Case "CM24", "CM14", "CM24_5BNS", "CM14HC", "CM24_X_BNS"
            Call Traspsend("D201", 0, 0)
            Call msDELAY(0.5)
            Call Traspsend("810000", 0, 0)
    End Select
End Sub

    
Function int_check(aa As String)
    If aa = "00" Then int_check = "Not exec"
    If aa = "01" Then int_check = "OK"
    If aa = "10" Then int_check = "Fault"
    If aa = "11" Then int_check = "--"

End Function
Sub Attendi_Invio()
    
    Tasto = 0
    Do While Not Tasto = 13
        DoEvents
    Loop
    Form1.Show
End Sub
Function attendi_banconota() As Integer
    Call Traspsend("", 3, 15)
    If TRASPRIC <> "" Then attendi_banconota = 1 Else attendi_banconota = 0

End Function
   
Sub connessione()
    Call Traspin
End Sub
Function dec_hex(a) As String
    
    If a <> "" Then
    
    dec_hex = hex$(a): If Len(dec_hex) = 1 Then dec_hex = "0" + dec_hex
    dec_hex = dec_hex
    Else
    hex2$ = ""
    
    End If

End Function
Function dec_hex2(a, digit) As String
    
    If a <> "" Then
    
    dec_hex2 = hex(a):
    dec_hex2 = dec_hex2
    Else
    hex2$ = ""
    End If
    For a = Len(dec_hex2) To digit - 1
    dec_hex2 = "0" & dec_hex2
    Next
End Function

Sub fine()
    
    'If Hcon <> 0 Then
    '    Call TraspOut
    '    stato_com = InterDoDisconnect(Hcon)
    '    Hcon = 0
    If Hcom > 0 Then Close_com
    End

End Sub

Function METTE_0(pippo As String)
    For a = 1 To Len(pippo)
        stringa = stringa & "0" + Mid$(pippo, a, 1)
    Next
    METTE_0 = stringa
End Function

Function TOGLIE_0(pippo As String)
    For a = 2 To Len(pippo) Step 2
        stringa = stringa + Mid$(pippo, a, 1)
    Next
    TOGLIE_0 = stringa
End Function

Sub msDELAY(tempoS)

    START = Timer 'Partenza del cronometro
    
    Do While Not Timer > START + tempoS
        DoEvents
    Loop

End Sub

Sub Traspin()
    If Macchina.Categoria <> "CIMA" Then
ripeti:
        
        Transparent_ON = False
        Send$ = "X,1"
        Call HiLevSend(Send$, OUT$)
        START = Timer 'Partenza del cronometro
        If OKCOMM = False Then
        Else
            If Right(Ricez$, 2) = ",4" Or Right(Ricez$, 3) = "104" Then
                OKCOMM = False
            Else
                OKCOMM = True
                Transparent_ON = True
            End If
        End If
    End If
End Sub
Sub Traspout()
If Macchina.Categoria <> "CIMA" Then
    Transparent_ON = False
    Send$ = "Y,1"
    
    Call HiLevSend(Send$, OUT$)
    
    If OKCOMM = False Then
        Transparent_ON = True
        Exit Sub
        
    Else
        Transparent_ON = False
    End If

End If
End Sub

Function Stringa_Invio(stringa As String) As String

For a = 1 To Len(stringa) Step 2
   
    Stringa_Invio = Stringa_Invio + Chr$("&h" + (Mid$(stringa, a, 2)))
Next

End Function
Function stringa_ricezione(stringa As String) As String

If stringa <> "" Then
    For a = 1 To Len(stringa)
        aaa = hex$(Asc(Mid$(stringa, a, 1)))
        If Len(aaa) = 1 Then aaa = "0" + aaa
        stringa_ricezione = stringa_ricezione + aaa
    Next
End If

End Function

Function cima_stringa(stringa As String, Nricez)
cima_stringa = ""
For a = 2 To Len(stringa) Step 3
    
    cima_stringa = cima_stringa & Mid$(stringa, a, 2)
Next
    cima_stringa = Left$(cima_stringa, Nricez)
End Function


Sub cambio_velocita(velocita As Integer, invio As Boolean)
    If direct = True Then
        If Reader_family < 3 Then
            If velocita = 4 Then velocita = 3
        End If
        If velocita < 4 Then
            TRASPRIC = ""
            If invio = True Then
                Call Attendi_per("41", reader_id_485_comunication)
                Call Traspsend("D3470" + Trim(str(velocita)), 0, 0)
        
            End If
            Close_com
            Call msDELAY(0.1)
            Select Case velocita
                Case 0
                B_Rate = 9600
                Case 1
                B_Rate = 19200
                Case 2
                B_Rate = 38400
                Case 3
                B_Rate = 57600
                Case Else
                B_Rate = 9600
            End Select
            Open_com
            Call msDELAY(0.3)
            
            If invio = True Then
                'Call ricevi(1, 5)
                Call Attendi_per("41", reader_id_485_comunication)
            End If
        Else
            Call Traspsend("D37700", 0, 0)
            'Call ricevi(1, 6)
            msDELAY (0.2)
            Close_com
            Call msDELAY(0.1)
            B_Rate = 115200
            Open_com
            Call msDELAY(0.3)
            Call Attendi_per("41", reader_id_485_comunication)
        End If
    Else
            Close_com
            Call msDELAY(0.1)
            Select Case velocita
                Case 0
                B_Rate = 9600
                Case 1
                B_Rate = 19200
                Case 2
                B_Rate = 38400
                Case 3
                B_Rate = 57600
                Case Else
                B_Rate = 9600
            End Select
            Open_com
            Call msDELAY(0.3)
    End If
End Sub

Sub ricezione(Nricez As Integer, Tout As Single)

    'Call ricevi(CLng(Nricez), Tout)
    
End Sub
Sub Attendi_per(stato, ind As String)

    If direct = True Then
        stato = Chr(Val("&h" & stato))
    End If
    TRASPRIC = stato
    Do While Not TRASPRIC <> stato
        
        Call Traspsend("F" & ind, 1, 0.5)
        If Mid$(TRASPRIC, 1, 1) = Chr(0) Then
            TRASPRIC = stato
        End If
    
            If Len(TRASPRIC) > 2 Then
                TRASPRIC = stato
            End If
    
        
        msDELAY (0.5)
        DoEvents
    Loop
    Form1.Show

End Sub

Function Attendi_per_T(stato As String, lop As Single, t As Single, ind) As Boolean
    
    TRASPRIC = stato
    
    Sta = Timer
    Do While Not TRASPRIC <> stato
    If Sta + t + lop < Timer Then
        Attendi_per_T = False
        Exit Function
    End If
    Call Traspsend("F" & ind, 1, 0.5)
    If Mid$(TRASPRIC, 1, 1) = Chr(0) Then
    TRASPRIC = stato
    End If
    If direct = True Then
        If Len(TRASPRIC) > 2 Then
            TRASPRIC = stato
        End If
    Else
        If Len(TRASPRIC) > 2 Then
            TRASPRIC = stato
        End If
    End If
    
    msDELAY (lop)
    DoEvents
    Loop
    If TRASPRIC = "" Then Attendi_per_T = False: Exit Function
    
    Attendi_per_T = True
    Form1.Show
End Function

Function int_offset(st As String) As Integer

    If Mid$(st, 2, 1) = "0" Then
        int_offset = -(Val("&h" + Mid$(st, 3, 2)))
    Else
        int_offset = (Val("&h" + Mid$(st, 3, 2)))
    End If

End Function

Sub transparent(traout As String)

End Sub
Public Sub Reboot()
    
    Call Traspsend("D37D", 0, 0)
    Call msDELAY(1.5)
    Call Attendi_per("41", reader_id_485_comunication)
    aa = TRASPRIC
    Call msDELAY(0.2)
    Call msDELAY(0.2)
    Call Traspsend("F3", 1, 2)
    Call Velocita_risposta_lettore
End Sub

Sub Velocita_risposta_lettore()
    If direct = True Then
        Call Traspsend("D31300", 0, 0)
    Else
        If Macchina.Categoria = "CM18" Or Macchina.Categoria = "CM18T" Or Macchina.Categoria = "CM18EVOT" Or _
        Macchina.Categoria = "CM20" Or Macchina.Categoria = "CM20T" Or Macchina.Categoria = "CM20S" Or _
        Macchina.Categoria = "CM18B" Or Macchina.Categoria = "CM18EVO" Or Macchina.Categoria = "CM18SOLO" _
        Or Macchina.Categoria = "CM18SOLOT" Or Macchina.Categoria = "OM61" Or Macchina.Categoria = "CM18HC" Then
    
             Call Traspsend("D31300", 0, 0)
        End If
    End If

End Sub

Public Function dec_asci(ByVal value As Integer, Optional ByVal numCaratteri As Integer = 4, Optional carattereVuoto As String = "0") As String
    dec_asci = ""
    Dim strValue As String
    strValue = value
    For i = 1 To Len(strValue)
        dec_asci = dec_asci & hex(Asc(Mid(strValue, i, 1)))
    Next
    For i = 1 To numCaratteri - Len(strValue)
        dec_asci = hex(Asc(carattereVuoto)) & dec_asci
    Next
    
End Function

Public Function dec_bin(deci As Variant)

    'On Error Resume Next
    dec_bin = ""
    Dim aa As Variant
    Do While Not deci = 0
    
        aa = Int((deci) * 1 / 2)
        DoEvents
        If aa * 2 = Val(deci) Then
            dec_bin = "0" + dec_bin
        Else
            dec_bin = "1" + dec_bin
        End If
        deci = Int(deci / 2)
    Loop
    
    If dec_bin = "" Then dec_bin = "0"
    
    For a = 1 To 8 - Len(dec_bin)
        DoEvents
        dec_bin = "0" + Trim(dec_bin)
    Next

End Function
Public Function bin_dec(bin)
    
    For a = 0 To Len(bin) - 1
        DoEvents
        aa = aa + Val(Mid$(bin, Len(bin) - a, 1) * 2 ^ a)
    Next
    
    bin_dec = aa

End Function

Public Function hex_bin(hex)

    For a = 1 To Len(hex)
    DoEvents
    b = dec_bin("&h" + Mid$(hex, a, 1))
    b = Mid$(b, 5)
    
    hex_bin = hex_bin + b
    Next

End Function



Public Function bin_hex(bin)
    If bin = "0" Then bin_hex = "0": Exit Function
    Do While Not Len(bin) Mod 4 = 0
        bin = "0" + bin
        DoEvents
    Loop
    
    For a = 1 To Len(bin) Step 4
        DoEvents
        p = hex(bin_dec(Mid$(bin, a, 4)))
        bin_hex = Trim((bin_hex + p))
    Next

End Function

Public Function InterpretaHiLevSend()
    Dim Virgola, indice, ultimaVirgola As Integer
    Virgola = 1
    indice = 1
    ultimaVirgola = 1
    
    For i = 1 To Len(Ricez)
        Virgola = InStr(i, Ricez, ",", vbTextCompare)
        If Virgola > i Then
            ultimaVirgola = Virgola
            comandoSingoloRisposta(indice) = Mid(Ricez, i, Virgola - i)
            indice = indice + 1
            i = ultimaVirgola
        End If
        If i = Len(Ricez) Then
            comandoSingoloRisposta(indice) = Mid(Ricez, ultimaVirgola + 1, Len(Ricez) - ultimaVirgola)
        End If
    Next
End Function

Sub HiLevSend(Send$, OUT$, Optional download_suite As Boolean = False, Optional ByVal Tout As Long = 5000)
    frmComScope.lstComScope.AddItem ("SND: " & Send$)
    frmComScope.lstComScope.ListIndex = frmComScope.lstComScope.ListCount - 1
    Ricez$ = ""
    Dim reply As Integer
    TransparentCommand.buff = Send$
    TransparentCommand.size = Len(TransparentCommand.buff)
    TransparentReply.buff = String(1700, "&H00")
    TransparentReply.size = Len(TransparentReply.buff)
    reply = CMCommand_Execute(CM_SINGLE_CONNECTION, SINGLECMD, TransparentCommand, TransparentReply, Tout)
    If reply = 0 Then
        OKCOMM = True
        TransparentReply.buff = Replace(TransparentReply.buff, "&", "")
        TransparentReply.buff = Replace(TransparentReply.buff, Chr(0), "")
        Ricez$ = TransparentReply.buff
        OUT$ = Ricez$
    End If
    frmComScope.lstComScope.AddItem ("RCV: " & Ricez$)
    frmComScope.lstComScope.ListIndex = frmComScope.lstComScope.ListCount - 1
    Call InterpretaHiLevSend
End Sub


Sub HiLevCommand(Send$, OUT$, Optional answer As Boolean = False)
frmComScope.lstComScope.AddItem ("SND: " & Send$)

Ricez$ = ""
Style = vbYesNo + vbCritical + vbDefaultButton3 ' Define buttons.
Title = "Errore comunicazione"  ' Define title.
Ctxt = 1000 ' Define topic
msg = Mex(295) ' Define message.'Connessione fallita. Riprovare?

OUT$ = Chr$(&H2) + Chr$(&H31) + Send$ + Chr$(&H3)

time_inizio = Timer

s% = 0
D% = 0
n% = Len(OUT$)

For aaa% = 2 To n% Step 1
    s% = (Asc(Mid$(OUT$, aaa%, 1)))
    D% = (D% + s%)
Next
R = (D% And &H7F)
OUT$ = OUT$ + Chr$(R)

aa:

    If Timer > time_inizio + 3600 Then
        response = MsgBox(msg, Style, Title, Help, Ctxt)
        Select Case response
        Case 6: GoTo aa
        Case 7: GoTo fine
        End Select
    End If

dl = 0
OKCOMM = False

Call Traspsend(Chr$(&H5), 2, 2.5) 'invio "ENQ"


If Controllo_Risposta("1030") = False Then
    If download_suite = True Then GoTo aa
    response = MsgBox(msg, Style, Title, Help, Ctxt)
    Select Case response
        Case 6: GoTo aa
        Case 7: GoTo fine
    End Select
End If

    Call Traspsend(OUT$, 2, 2.5)

    START = Timer


If Controllo_Risposta("1031") = False Then
    If download_suite = True Then GoTo aa
    response = MsgBox(msg, Style, Title, Help, Ctxt)
    Select Case response
        Case 6: GoTo aa
        Case 7: GoTo fine
    End Select
End If


fine:

End Sub

Function Controllo_Risposta(risposta As String) As Boolean
Style = vbYesNo + vbCritical + vbDefaultButton3 ' Define buttons.
Title = "Errore comunicazione"  ' Define title.
Ctxt = 1000 ' Define topic
If TRASPRIC <> risposta Then

    If Len(TRASPRIC) = 0 Then 'Controllo che ci sia stata la risposta
        
        Controllo_Risposta = False

    End If

    If TRASPRIC = Chr$(&H15) Then 'Controllo che la risposta non sia BUSY
        
       Controllo_Risposta = False

    End If
Else

    Controllo_Risposta = True

End If

End Function

Public Function hex_asc(stringa)

For a = 1 To Len(stringa) Step 2
    hex_asc = hex_asc + Chr("&h" + (Mid$(stringa, a, 2)))
Next

End Function

Public Function hex_dec(stringa)

For a = 1 To Len(stringa) Step 2
    hex_dec = hex_dec + str(Val("&h" + (Mid$(stringa, a, 2)))) & " "
Next

End Function

Public Sub Ext_Trig()
Dim n As Long
Call Traspsend("D355", 1, 5)
If direct = True Then aa = stringa_ricezione(TRASPRIC) Else aa = TRASPRIC
If aa = "00" Then
Call Traspsend("D35401", n, 3)
End If
End Sub
Public Sub Int_Trig()
Dim n As Long

Call Traspsend("D355", 1, 5)
If direct = True Then aa = stringa_ricezione(TRASPRIC) Else aa = TRASPRIC
If aa = "01" Then
Call Traspsend("D35400", n, 3)
End If
End Sub
Public Sub Auto_Answ_ON()
    Dim n As Long
    Call Traspsend("D353", 1, 5)
    If direct = True Then aa = stringa_ricezione(TRASPRIC) Else aa = TRASPRIC
    If aa = "00" Then
        Call Traspsend("D35201", n, 3)
    End If
End Sub

Public Sub Auto_Answ_Off()
Dim n As Long

Call Traspsend("D353", 1, 5)
If direct = True Then aa = stringa_ricezione(TRASPRIC) Else aa = TRASPRIC
If aa = "01" Then
Call Traspsend("D35200", n, 3)
End If
End Sub

Public Sub Dbl_On()

Dim n As Long

Call Traspsend("D349", 1, 5)
If direct = True Then aa = stringa_ricezione(TRASPRIC) Else aa = TRASPRIC
If aa = "00" Then
Call Traspsend("D34801", n, 3)
End If
End Sub

Public Sub Dbl_Off()
Dim n As Long

Call Traspsend("D349", 1, 5)
If direct = True Then aa = stringa_ricezione(TRASPRIC) Else aa = TRASPRIC
If aa = "01" Then
Call Traspsend("D34800", n, 3)
End If
End Sub

Public Sub CmpMode_CM()
Dim n As Long

Call Traspsend("D35F", 1, 5)
If direct = True Then
    aa = Right(stringa_ricezione(TRASPRIC), 1)
Else
    aa = Right(TRASPRIC, 1)
End If
If aa = "0" Then
    Call Traspsend("D35E01", n, 3)
End If
End Sub

Public Sub CmpMode_CDM()
Dim n As Long
1
Call Traspsend("D35F", 1, 5)
If direct = True Then aa = stringa_ricezione(TRASPRIC) Else aa = TRASPRIC
If aa = "01" Then
Call Traspsend("D35E00", n, 3)
End If
End Sub

Public Sub ExtFitness_ON()
    Dim n As Long
    
    Call Traspsend("D37001", 1, 5)
    If direct = True Then
        aa = stringa_ricezione(TRASPRIC)
    Else:
        aa = TRASPRIC
    End If
    If aa = "00" Then
        Call Traspsend("D3700001", n, 3)
    End If
End Sub

Public Sub ExtFitness_OFF()
    Dim n As Long
    
    Call Traspsend("D37001", 1, 5)
    If direct = True Then aa = stringa_ricezione(TRASPRIC) Else aa = TRASPRIC
    If aa = "01" Then
    Call Traspsend("D3700000", n, 3)
    End If
End Sub

Function Int_Mag(stri As String)
    Int_Mag = ""
    For a = 3 To Len(stri) Step 4
        Int_Mag = Int_Mag & Mid$(TOGLIE_0(stringa_ricezione(stri)), a, 2)
    Next

End Function
Public Sub unzip(nomeFile As String, nomedir As String)
    Dim process_id As Long
    Dim process_handle As Long
    
    ' Start the program.
    'On Error GoTo ShellError
    process_id = Shell(Zip_Path & "7z x """ & nomeFile & """ " & """-o" & nomedir & "\" & """ *.* -aoa", vbNormalFocus)

 'On Error GoTo 0

    ' Hide.
    'Form1.Visible = False
    DoEvents

    ' Wait for the program to finish.
    ' Get the process handle.
    process_handle = OpenProcess(SYNCHRONIZE, 0, process_id)
    If process_handle <> 0 Then
        WaitForSingleObject process_handle, INFINITE
        CloseHandle process_handle
    End If
    'Form1.Visible = True
     

End Sub

Function ComputerName() As String
    Dim Buffer As String * 512, length As Long
    length = Len(Buffer)
    'GetComputerName returns zero on failure
    If GetComputerName(Buffer, length) Then
        ComputerName = Left$(Buffer, length)
    End If
End Function

Sub Aggiorna_Flag_Avanzamento(Optional RIMETTI_A_ZERO As Boolean = False)
    'D3.44.02.07.0x.0d.0d
    If Registra_Flag_avanzamento = True Then
        If Macchina.Lettore = "RS22" Or Macchina.Lettore = "RS32" Then
            If RIMETTI_A_ZERO = False Then
                Call Traspsend("D344020708" & METTE_0(TRASPRIC), 0, 0)
            Else
                Call Traspsend("D3440207080000", 0, 0)
            End If
            Call Attendi_per("41", 3)
        End If
    End If
End Sub

Function Leggi_Flag_Avanzamento() As String
If Macchina.Lettore = "RS22" Or Macchina.Lettore = "RS32" Then

    Call Traspsend("D343020708", 2, 5)
    Leggi_Flag_Avanzamento = TOGLIE_0(TRASPRIC)
End If
End Function

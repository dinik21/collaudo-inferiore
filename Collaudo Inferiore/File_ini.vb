﻿Module File_ini
    Private Declare Unicode Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringW" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Int32, ByVal lpFileName As String) As Int32
    Private Declare Unicode Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringW" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Long
    Dim Valore_letto As String

    Public Function Read_Setup(ByVal Sezione As String, ByVal Parametro As String) As String

        Dim FileName As String
        Dim mode As String
        Dim aa As Object
        FileName = Application.StartupPath + "\setup.ini"

        mode = "GET"

        aa = ReadWriteINI(mode, Sezione, Parametro, FileName, "")
        Read_Setup = Valore_letto

    End Function
    Public Function Read_Bitmap(ByVal Sezione As String, ByVal Parametro As String) As String

        Dim FileName As String
        Dim mode As String
        Dim aa As Object
        FileName = Application.StartupPath + "BitmapViewerRS32.ini"
        mode = "GET"

        aa = ReadWriteINI(mode, Sezione, Parametro, FileName, "")
        Read_Bitmap = Valore_letto

    End Function

    Public Function Read_Ini(ByVal File As String, ByVal Sezione As String, ByVal Parametro As String) As String
        Dim FileName As String
        Dim mode As String
        Dim aa As Object
        FileName = Application.StartupPath + "\" + File
        mode = "GET"

        aa = ReadWriteINI(mode, Sezione, Parametro, FileName, "")
        Read_Ini = Valore_letto
    End Function

    Public Function Read_ParPiastra(ByVal Sezione As String, ByVal Parametro As String) As String

        Dim FileName As String
        Dim mode As String
        Dim aa As Object
        FileName = Application.StartupPath + "Rs2x_" & ".ini"
        mode = "GET"

        aa = ReadWriteINI(mode, Sezione, Parametro, FileName, "")
        Read_ParPiastra = Valore_letto

    End Function

    Public Function ReadWriteINI(ByVal mode As String, ByVal tmpSecname As String, ByVal tmpKeyname As String, ByVal FileName As String, ByVal tmpKeyValue As Object) As String
        Valore_letto = ""

        Dim tmpString As String

        ReadWriteINI = "OK"

        If Len(mode) = 0 Then
            ReadWriteINI = "ERROR MODE"
            Exit Function
        End If

        If Len(tmpSecname) = 0 Then
            ReadWriteINI = "ERROR Secname"
            Exit Function
        End If

        If Len(tmpKeyname) = 0 Then
            ReadWriteINI = "ERROR Keyname"
            Exit Function
        End If

        If UCase(mode) = "WRITE" Then
            If Len(tmpKeyValue) = 0 Then
                ReadWriteINI = "ERROR KeyValue"
                Exit Function
            Else
                Dim secname
                Dim KeyName As String
                Dim keyvalue As String

                secname = tmpSecname
                KeyName = tmpKeyname
                keyvalue = tmpKeyValue
                Dim anInt As String
                anInt = WritePrivateProfileString(secname, KeyName, keyvalue, FileName)
            End If
        End If
        If UCase(mode) = "GET" Then
            Dim secname
            Dim KeyName As String
            Dim keyvalue As String
            Dim defaultkey As String
            Dim anInt As String
            Dim aa As Integer

            secname = tmpSecname
            KeyName = tmpKeyname
            defaultkey = "Failed"
            keyvalue = StrDup(2048, Chr(32))
            anInt = GetPrivateProfileString(secname, KeyName, defaultkey, keyvalue, Len(keyvalue), FileName)
            If Left(keyvalue, 6) = "Failed" Then
                ReadWriteINI = "Error failed"
                tmpString = keyvalue
                tmpString = RTrim(tmpString)
                tmpString = Left(tmpString, Len(tmpString) - 1)
                'Throw New IO.FileLoadException("Errore lettura file ini:" & Chr(13) & "Nome_file:" & FileName & Chr(13) & "Sezione:" & tmpSecname & "  Campo:" & tmpKeyname & Chr(13) & "L'applicazione verrà chiusa, verificare il file .Ini indicato e riavviare l'applicazione")
            End If
            Valore_letto = (keyvalue.Remove(keyvalue.Trim.Length - 1)).ToString
            aa = Valore_letto.Length
        End If
        Exit Function


    End Function
End Module

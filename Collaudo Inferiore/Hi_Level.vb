﻿Module Hi_Level


    Structure ope
        Dim RC As String
    End Structure

    Public CMD_Open As ope
    Public CMD_tra_in As ope
    Public CMD_tra_out As ope

    Structure cass
        Dim cassatte_address As String
        Dim status As String
    End Structure

    Structure ext_sta

        Dim RC As String
        Dim Feeder As String
        Dim Controller As String
        Dim Safe As String
        Dim Reader As String
        Dim Cassette1 As cass
        Dim Cassette2 As cass
        Dim Cassette3 As cass
        Dim Cassette4 As cass
        Dim Cassette5 As cass
        Dim Cassette6 As cass
        Dim Cassette7 As cass
        Dim Cassette8 As cass
        Dim Cassette9 As cass
        Dim Cassette10 As cass
        Dim Cassette11 As cass
        Dim Cassette12 As cass

    End Structure

    Structure reply_c
        Dim Description As String
        Dim Cause As String
        Dim Zone As String
    End Structure

    Structure Cash_d
        Dim nome As String
        Dim N_note As Integer
        Dim denomination As String
        Dim Enabled As String
        Dim Capacity As Integer
    End Structure


    Structure dep
        Dim RC As String
        Dim Banconote_tot As Byte
        Dim Banconote_Rif As Byte
        Dim Banconote_UNFit As Byte
        Dim Bn_cas() As Banc_cas
    End Structure

    Structure PREL
        Dim RC As String
        Dim Bn_cas() As Banc_cas
    End Structure

    Structure CASH
        Dim RC As String
        Dim Bn_cas() As Cash_d
    End Structure

    Public Cash_dat As CASH
    Public Deposito As dep
    Public prelievo As dep
    Public Ext_Status As ext_sta
    Public reply_code As reply_c

    Sub Extended_Stat()

        Ext_Status.RC = ""
        Ext_Status.Feeder = ""
        Ext_Status.Controller = ""
        Ext_Status.Reader = ""
        Ext_Status.Cassette1.cassatte_address = ""
        Ext_Status.Cassette1.status = ""
        Ext_Status.Cassette2.cassatte_address = ""
        Ext_Status.Cassette2.status = ""
        Ext_Status.Cassette3.cassatte_address = ""
        Ext_Status.Cassette3.status = ""
        Ext_Status.Cassette4.cassatte_address = ""
        Ext_Status.Cassette4.status = ""
        Ext_Status.Cassette5.cassatte_address = ""
        Ext_Status.Cassette5.status = ""
        Ext_Status.Cassette6.cassatte_address = ""
        Ext_Status.Cassette6.status = ""
        Ext_Status.Cassette7.cassatte_address = ""
        Ext_Status.Cassette7.status = ""
        Ext_Status.Cassette8.cassatte_address = ""
        Ext_Status.Cassette8.status = ""
        Ext_Status.Cassette9.cassatte_address = ""
        Ext_Status.Cassette9.status = ""
        Ext_Status.Cassette10.cassatte_address = ""
        Ext_Status.Cassette10.status = ""
        Ext_Status.Cassette11.cassatte_address = ""
        Ext_Status.Cassette11.status = ""
        Ext_Status.Cassette12.cassatte_address = ""
        Ext_Status.Cassette12.status = ""

        cmdComandoSingolo("E,1", 2)
        If comandoSingoloRisposta.Count < 10 Then Exit Sub

        If comandoSingoloRisposta(2) = "101" Or comandoSingoloRisposta(2) = "1" Then
            Ext_Status.RC = comandoSingoloRisposta(2)
            For i = 3 To comandoSingoloRisposta.Count - 1
                Select Case comandoSingoloRisposta(i).Substring(0, 1)
                    Case "1" 'Feeder
                        Ext_Status.Feeder = comandoSingoloRisposta(i).Substring(1, 2)
                    Case "2" 'Controller
                        Ext_Status.Controller = comandoSingoloRisposta(i).Substring(1, 2)
                    Case "3" 'Reader
                        Ext_Status.Reader = comandoSingoloRisposta(i).Substring(1, 2)
                    Case "4" 'safe CM18T
                        Ext_Status.Safe = comandoSingoloRisposta(i).Substring(1, 2)
                    Case "A" 'Cassette
                        Ext_Status.Cassette1.cassatte_address = comandoSingoloRisposta(i).Substring(0, 1)
                        Ext_Status.Cassette1.status = comandoSingoloRisposta(i).Substring(1, 2)
                    Case "B" 'Cassette
                        Ext_Status.Cassette2.cassatte_address = comandoSingoloRisposta(i).Substring(0, 1)
                        Ext_Status.Cassette2.status = comandoSingoloRisposta(i).Substring(1, 2)
                    Case "C" 'Cassette
                        Ext_Status.Cassette3.cassatte_address = comandoSingoloRisposta(i).Substring(0, 1)
                        Ext_Status.Cassette3.status = comandoSingoloRisposta(i).Substring(1, 2)
                    Case "D" 'Cassette
                        Ext_Status.Cassette4.cassatte_address = comandoSingoloRisposta(i).Substring(0, 1)
                        Ext_Status.Cassette4.status = comandoSingoloRisposta(i).Substring(1, 2)
                    Case "E" 'Cassette
                        Ext_Status.Cassette5.cassatte_address = comandoSingoloRisposta(i).Substring(0, 1)
                        Ext_Status.Cassette5.status = comandoSingoloRisposta(i).Substring(1, 2)
                    Case "F" 'Cassette
                        Ext_Status.Cassette6.cassatte_address = comandoSingoloRisposta(i).Substring(0, 1)
                        Ext_Status.Cassette6.status = comandoSingoloRisposta(i).Substring(1, 2)
                    Case "G" 'Cassette
                        Ext_Status.Cassette7.cassatte_address = comandoSingoloRisposta(i).Substring(0, 1)
                        Ext_Status.Cassette7.status = comandoSingoloRisposta(i).Substring(1, 2)
                    Case "H" 'Cassette
                        Ext_Status.Cassette8.cassatte_address = comandoSingoloRisposta(i).Substring(0, 1)
                        Ext_Status.Cassette8.status = comandoSingoloRisposta(i).Substring(1, 2)
                    Case "J" 'Cassette
                        Ext_Status.Cassette9.cassatte_address = comandoSingoloRisposta(i).Substring(0, 1)
                        Ext_Status.Cassette9.status = comandoSingoloRisposta(i).Substring(1, 2)
                    Case "K" 'Cassette
                        Ext_Status.Cassette10.cassatte_address = comandoSingoloRisposta(i).Substring(0, 1)
                        Ext_Status.Cassette10.status = comandoSingoloRisposta(i).Substring(1, 2)
                    Case "L" 'Cassette
                        Ext_Status.Cassette11.cassatte_address = comandoSingoloRisposta(i).Substring(0, 1)
                        Ext_Status.Cassette11.status = comandoSingoloRisposta(i).Substring(1, 2)

                End Select
            Next


        End If

        'Call HiLevSend("E,1", "")

        'Dim aaa As String = Ricez$ & ","
        'Dim prev As Integer = 1
        'Dim act As Integer = 1
        'Dim virgola As Integer = 0
        'For a = 1 To Len(aaa)

        '    If Mid$(aaa, a, 1) = "," Then
        '        act = a
        '        virgola = virgola + 1
        '        Dim aa As String = Mid$(aaa, prev, act - prev)
        '        Select Case virgola
        '            Case 3
        '                Ext_Status.RC = aa
        '            Case 4
        '                Ext_Status.Feeder = Mid$(aa, 2, 2)
        '            Case 5
        '                Ext_Status.Controller = Mid$(aa, 2, 2)
        '            Case 6
        '                Ext_Status.Reader = Mid$(aa, 2, 2)
        '            Case 7
        '                If Mid$(aa, 1, 1) = "4" Then
        '                    Ext_Status.Safe = Mid$(aa, 2, 2)
        '                Else
        '                    Ext_Status.Safe = ""
        '                    Ext_Status.Cassette1.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette1.status = Mid$(aa, 2, 2)
        '                End If
        '            Case 8
        '                If Ext_Status.Safe = "" Then
        '                    Ext_Status.Cassette2.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette2.status = Mid$(aa, 2, 2)
        '                Else
        '                    Ext_Status.Cassette1.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette1.status = Mid$(aa, 2, 2)
        '                End If
        '            Case 9
        '                If Ext_Status.Safe = "" Then
        '                    Ext_Status.Cassette3.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette3.status = Mid$(aa, 2, 2)
        '                Else
        '                    Ext_Status.Cassette2.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette2.status = Mid$(aa, 2, 2)
        '                End If
        '            Case 10
        '                If Ext_Status.Safe = "" Then
        '                    Ext_Status.Cassette4.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette4.status = Mid$(aa, 2, 2)
        '                Else
        '                    Ext_Status.Cassette3.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette3.status = Mid$(aa, 2, 2)
        '                End If

        '            Case 11
        '                If Ext_Status.Safe = "" Then
        '                    Ext_Status.Cassette5.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette5.status = Mid$(aa, 2, 2)
        '                Else
        '                    Ext_Status.Cassette4.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette4.status = Mid$(aa, 2, 2)
        '                End If

        '            Case 12
        '                If Ext_Status.Safe = "" Then
        '                    Ext_Status.Cassette6.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette6.status = Mid$(aa, 2, 2)
        '                Else
        '                    Ext_Status.Cassette5.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette5.status = Mid$(aa, 2, 2)
        '                End If

        '            Case 13
        '                If Ext_Status.Safe = "" Then
        '                    Ext_Status.Cassette7.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette7.status = Mid$(aa, 2, 2)
        '                Else
        '                    Ext_Status.Cassette6.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette6.status = Mid$(aa, 2, 2)
        '                End If
        '            Case 14
        '                If Ext_Status.Safe = "" Then
        '                    Ext_Status.Cassette8.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette8.status = Mid$(aa, 2, 2)
        '                Else
        '                    Ext_Status.Cassette7.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette7.status = Mid$(aa, 2, 2)
        '                End If
        '            Case 15
        '                If Ext_Status.Safe = "" Then
        '                    Ext_Status.Cassette9.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette9.status = Mid$(aa, 2, 2)
        '                Else
        '                    Ext_Status.Cassette8.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette8.status = Mid$(aa, 2, 2)
        '                End If
        '            Case 16
        '                If Ext_Status.Safe = "" Then
        '                    Ext_Status.Cassette10.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette10.status = Mid$(aa, 2, 2)
        '                Else
        '                    Ext_Status.Cassette9.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette9.status = Mid$(aa, 2, 2)
        '                End If
        '            Case 17
        '                If Ext_Status.Safe = "" Then
        '                    Ext_Status.Cassette11.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette11.status = Mid$(aa, 2, 2)
        '                Else
        '                    Ext_Status.Cassette10.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette10.status = Mid$(aa, 2, 2)
        '                End If
        '            Case 18
        '                If Ext_Status.Safe = "" Then
        '                    Ext_Status.Cassette12.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette12.status = Mid$(aa, 2, 2)
        '                Else
        '                    Ext_Status.Cassette11.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette11.status = Mid$(aa, 2, 2)
        '                End If
        '            Case 19
        '                If Ext_Status.Safe <> "" Then
        '                    Ext_Status.Cassette12.cassatte_address = Mid$(aa, 1, 1)
        '                    Ext_Status.Cassette12.status = Mid$(aa, 2, 2)
        '                End If
        '        End Select
        '        prev = act + 1
        '    End If

        'Next

    End Sub

    Public Sub Carica_Reply_Code(ByVal Sezione As String, ByVal RC As String)



    End Sub

    Sub Version()

        Dim Entra_in_Transparent As Boolean = False

        If Transparent_ON = True Then
            Traspout()
            Entra_in_Transparent = True
        End If

        Call HiLevSend("T,1,6,13,4", "")

        Dim aaa As String = Ricez$ & ","
        Dim prev As Integer = 1
        Dim act As Integer = 1
        Dim virgola As Integer = 0

        For a = 1 To Len(aaa)

            If Mid$(aaa, a, 1) = "," Then
                act = a
                virgola = virgola + 1
                Dim aa As String = Mid$(aaa, prev, act - prev)
                Select Case virgola
                    Case 3
                        Ext_Status.RC = aa
                    Case 4
                        Ext_Status.Feeder = Mid$(aa, 2, 2)
                    Case 5
                        Ext_Status.Controller = Mid$(aa, 2, 2)
                    Case 6
                        Ext_Status.Reader = Mid$(aa, 2, 2)
                    Case 7
                        Ext_Status.Cassette1.cassatte_address = Mid$(aa, 1, 1)
                        Ext_Status.Cassette1.status = Mid$(aa, 2, 2)
                    Case 8
                        Ext_Status.Cassette2.cassatte_address = Mid$(aa, 1, 1)
                        Ext_Status.Cassette2.status = Mid$(aa, 2, 2)
                    Case 9
                        Ext_Status.Cassette3.cassatte_address = Mid$(aa, 1, 1)
                        Ext_Status.Cassette3.status = Mid$(aa, 2, 2)
                    Case 10
                        Ext_Status.Cassette4.cassatte_address = Mid$(aa, 1, 1)
                        Ext_Status.Cassette4.status = Mid$(aa, 2, 2)
                    Case 11
                        Ext_Status.Cassette5.cassatte_address = Mid$(aa, 1, 1)
                        Ext_Status.Cassette5.status = Mid$(aa, 2, 2)
                    Case 12
                        Ext_Status.Cassette6.cassatte_address = Mid$(aa, 1, 1)
                        Ext_Status.Cassette6.status = Mid$(aa, 2, 2)
                    Case 13
                        Ext_Status.Cassette7.cassatte_address = Mid$(aa, 1, 1)
                        Ext_Status.Cassette7.status = Mid$(aa, 2, 2)
                    Case 14
                        Ext_Status.Cassette8.cassatte_address = Mid$(aa, 1, 1)
                        Ext_Status.Cassette8.status = Mid$(aa, 2, 2)
                    Case 15
                        Ext_Status.Cassette9.cassatte_address = Mid$(aa, 1, 1)
                        Ext_Status.Cassette9.status = Mid$(aa, 2, 2)
                    Case 16
                        Ext_Status.Cassette10.cassatte_address = Mid$(aa, 1, 1)
                        Ext_Status.Cassette10.status = Mid$(aa, 2, 2)
                    Case 17
                        Ext_Status.Cassette11.cassatte_address = Mid$(aa, 1, 1)
                        Ext_Status.Cassette11.status = Mid$(aa, 2, 2)
                    Case 18
                        Ext_Status.Cassette12.cassatte_address = Mid$(aa, 1, 1)
                        Ext_Status.Cassette12.status = Mid$(aa, 2, 2)
                End Select
                prev = act + 1
            End If

        Next

        If Entra_in_Transparent = True Then Traspin()




    End Sub

    Function cash_data()

        ReDim Cash_dat.Bn_cas(12)

        Dim kk(100) As Object
        Call frmPrincipale.Open_R(False)
        'Call HiLevSend("G,1,R", "")
        cmdComandoSingolo("G,1,R", 3, 5000)


        If comandoSingoloRisposta.Count > 10 Then
            Dim idCass As Integer = 0
            For i = 4 To comandoSingoloRisposta.Count - 1 Step 5
                Cash_dat.Bn_cas(idCass).denomination = comandoSingoloRisposta(i)
                Cash_dat.Bn_cas(idCass).N_note = comandoSingoloRisposta(i + 1)
                Cash_dat.Bn_cas(idCass).Capacity = comandoSingoloRisposta(i + 2)
                Cash_dat.Bn_cas(idCass).nome = comandoSingoloRisposta(i + 3)
                Cash_dat.Bn_cas(idCass).Enabled = comandoSingoloRisposta(i + 4)
                idCass += 1
            Next

        End If
        'Dim aaa As String = Ricez$ & ","
        'Dim prev As Integer = 1
        'Dim act As Integer = 1
        'Dim virgola As Integer = 0

        'For a = 1 To Len(aaa)
        '    If Mid$(aaa, a, 1) = "," Then
        '        act = a
        '        virgola = virgola + 1
        '        kk(virgola) = Mid$(aaa, prev, act - prev)
        '        prev = act + 1
        '    End If

        'Next
        'Dim k As Integer = 0
        ''Ext_Status.rc = kk(4)
        'For a = 5 To 100 Step 5

        '    Cash_dat.Bn_cas(k).denomination = kk(a)
        '    Cash_dat.Bn_cas(k).N_note = kk(a + 1)
        '    Cash_dat.Bn_cas(k).Capacity = kk(a + 2)
        '    Cash_dat.Bn_cas(k).nome = kk(a + 3)
        '    Cash_dat.Bn_cas(k).Enabled = kk(a + 4)
        '    k = k + 1
        '    If Inferiore.categoria = "CM18" And k = 8 Then Exit For
        '    If Inferiore.categoria = "CM20" And k = 10 Then Exit For
        '    If Inferiore.categoria = "CM18T" And k = 12 Then Exit For
        '    If Inferiore.categoria = "CM18EVOT" And k = 12 Then Exit For
        '    If Inferiore.categoria = "CM20T" And k = 12 Then Exit For
        '    If Inferiore.categoria = "CM20S" And k = 8 Then Exit For
        '    If Inferiore.categoria = "CM18EVO" And k = 8 Then Exit For
        '    If Inferiore.categoria = "CM18B" And k = 6 Then Exit For
        'Next





    End Function

    Function Deposito_ALL(Optional ByVal counting As Boolean = False, Optional ByVal FitUnfit As Boolean = False) As Boolean


        Call frmPrincipale.Open_R(True)
        If counting = False Then
            Call HiLevSend("D,1,R,0,0000", "")
        Else
            If FitUnfit = True Then
                Call HiLevSend("D,1,R,10,0000", "")
            Else
                Call HiLevSend("D,1,R,3,0000", "")
            End If
        End If
        Call Interpreta_deposito(Ricez$)
        Deposito_ALL = True



    End Function

    Sub Interpreta_deposito(ByVal stringa As String)
        ReDim Deposito.Bn_cas(12)
        For a = 0 To 11
            Deposito.Bn_cas(a).Denomination = ""
            Deposito.Bn_cas(a).N_note = 0
        Next

        If comandoSingoloRisposta.Count > 4 Then
            Deposito.RC = comandoSingoloRisposta(3)
            Deposito.Banconote_tot = comandoSingoloRisposta(4)
            Deposito.Banconote_UNFit = comandoSingoloRisposta(5)
            Deposito.Banconote_Rif = comandoSingoloRisposta(6)
            Deposito.Bn_cas(0).Denomination = comandoSingoloRisposta(7)
            Deposito.Bn_cas(0).N_note = comandoSingoloRisposta(8)
            Deposito.Bn_cas(1).Denomination = comandoSingoloRisposta(9)
            Deposito.Bn_cas(1).N_note = comandoSingoloRisposta(10)
            Deposito.Bn_cas(2).Denomination = comandoSingoloRisposta(11)
            Deposito.Bn_cas(2).N_note = comandoSingoloRisposta(12)
            Deposito.Bn_cas(3).Denomination = comandoSingoloRisposta(13)
            Deposito.Bn_cas(3).N_note = comandoSingoloRisposta(14)
            Deposito.Bn_cas(4).Denomination = comandoSingoloRisposta(15)
            Deposito.Bn_cas(4).N_note = comandoSingoloRisposta(16)
            Deposito.Bn_cas(5).Denomination = comandoSingoloRisposta(17)
            Deposito.Bn_cas(5).N_note = comandoSingoloRisposta(18)
            Deposito.Bn_cas(6).Denomination = comandoSingoloRisposta(19)
            Deposito.Bn_cas(6).N_note = comandoSingoloRisposta(20)
            Deposito.Bn_cas(7).Denomination = comandoSingoloRisposta(21)
            Deposito.Bn_cas(7).N_note = comandoSingoloRisposta(22)
            Deposito.Bn_cas(8).Denomination = comandoSingoloRisposta(23)
            Deposito.Bn_cas(8).N_note = comandoSingoloRisposta(24)
            Deposito.Bn_cas(9).Denomination = comandoSingoloRisposta(25)
            Deposito.Bn_cas(9).N_note = comandoSingoloRisposta(26)
            Deposito.Bn_cas(10).Denomination = comandoSingoloRisposta(27)
            Deposito.Bn_cas(10).N_note = comandoSingoloRisposta(28)
            Deposito.Bn_cas(11).Denomination = comandoSingoloRisposta(29)
            Deposito.Bn_cas(11).N_note = comandoSingoloRisposta(30)

        End If

        'Dim aaa As String = Ricez$ & ","
        'Dim prev As Integer = 1
        'Dim act As Integer = 1
        'Dim virgola As Integer = 0
        'For a = 1 To Len(aaa)

        '    If Mid$(aaa, a, 1) = "," Then
        '        act = a
        '        virgola = virgola + 1
        '        Dim aa As String = Mid$(aaa, prev, act - prev)
        '        Select Case virgola
        '            Case 4
        '                Deposito.RC = aa
        '            Case 5
        '                Deposito.Banconote_tot = aa
        '            Case 6
        '                Deposito.Banconote_UNFit = aa
        '            Case 7
        '                Deposito.Banconote_Rif = aa
        '            Case 8
        '                Deposito.Bn_cas(0).Denomination = aa
        '            Case 9
        '                Deposito.Bn_cas(0).N_note = aa
        '            Case 10
        '                Deposito.Bn_cas(1).Denomination = aa
        '            Case 11
        '                Deposito.Bn_cas(1).N_note = aa
        '            Case 12
        '                Deposito.Bn_cas(2).Denomination = aa
        '            Case 13
        '                Deposito.Bn_cas(2).N_note = aa
        '            Case 14
        '                Deposito.Bn_cas(3).Denomination = aa
        '            Case 15
        '                Deposito.Bn_cas(3).N_note = aa
        '            Case 16
        '                Deposito.Bn_cas(4).Denomination = aa
        '            Case 17
        '                Deposito.Bn_cas(4).N_note = aa
        '            Case 18
        '                Deposito.Bn_cas(5).Denomination = aa
        '            Case 19
        '                Deposito.Bn_cas(5).N_note = aa
        '            Case 20
        '                Deposito.Bn_cas(6).Denomination = aa
        '            Case 21
        '                Deposito.Bn_cas(6).N_note = aa
        '            Case 22
        '                Deposito.Bn_cas(7).Denomination = aa
        '            Case 23
        '                Deposito.Bn_cas(7).N_note = aa
        '            Case 24
        '                Deposito.Bn_cas(8).Denomination = aa
        '            Case 25
        '                Deposito.Bn_cas(8).N_note = aa
        '            Case 26
        '                Deposito.Bn_cas(9).Denomination = aa
        '            Case 27
        '                Deposito.Bn_cas(9).N_note = aa
        '            Case 28
        '                Deposito.Bn_cas(10).Denomination = aa
        '            Case 29
        '                Deposito.Bn_cas(10).N_note = aa
        '            Case 30
        '                Deposito.Bn_cas(11).Denomination = aa
        '            Case 31
        '                Deposito.Bn_cas(11).N_note = aa

        '        End Select
        '        prev = act + 1
        '    End If

        'Next

    End Sub

    Function Mini_prelievo()
        ReDim prelievo.Bn_cas(12)
        Dim kk(100) As Object
        Call frmPrincipale.Open_R(True)

        If Inferiore.categoria = "CM18B" Then
            Call HiLevSend("W,6,R,CPCA,10,CPDA,10,CPEA,10,CPGA,10,CPHA,10,CP**,20", "")
        Else
            Call HiLevSend("W,6,R,CPCA,10,CPDA,10,CPEA,10,CPGA,10,CPHA,10,CPIA,10,CPJA,10", "")
        End If

        Dim aaa As String = Ricez$ & ","
        Dim prev As Integer = 1
        Dim act As Integer = 1
        Dim virgola As Integer = 0

        For a = 1 To Len(aaa)
            If Mid$(aaa, a, 1) = "," Then
                act = a
                virgola = virgola + 1
                kk(virgola) = Mid$(aaa, prev, act - prev)
                prev = act + 1
            End If

        Next
        Dim k As Integer = 0
        prelievo.RC = kk(4)
        For a = 5 To 100 Step 2

            prelievo.Bn_cas(k).Denomination = kk(a)
            prelievo.Bn_cas(k).N_note = kk(a + 1)

            k = k + 1
            If Inferiore.categoria = "CM18" And k = 8 Then Exit For
            If Inferiore.categoria = "CM20" And k = 10 Then Exit For
            If Inferiore.categoria = "CM18T" And k = 12 Then Exit For
            If Inferiore.categoria = "CM18EVOT" And k = 12 Then Exit For
            If Inferiore.categoria = "CM20T" And k = 12 Then Exit For
            If Inferiore.categoria = "CM20S" And k = 8 Then Exit For
            If Inferiore.categoria = "CM18EVO" And k = 8 Then Exit For
            If Inferiore.categoria = "CM18B" And k = 6 Then Exit For
            If k > 12 Then Exit For
        Next

    End Function



    Function Mette_4_zeri(ByVal aa As String) As String
        For a = Len(aa) + 1 To 4
            aa = "0" & aa
        Next
        Mette_4_zeri = aa
    End Function




End Module